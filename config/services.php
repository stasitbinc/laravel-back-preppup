<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Third Party Services
    |--------------------------------------------------------------------------
    |
    | This file is for storing the credentials for third party services such
    | as Mailgun, Postmark, AWS and more. This file provides the de facto
    | location for this type of information, allowing packages to have
    | a conventional file to locate the various service credentials.
    |
    */

    'mailgun' => [
        'domain' => env('MAILGUN_DOMAIN'),
        'secret' => env('MAILGUN_SECRET'),
        'endpoint' => env('MAILGUN_ENDPOINT', 'api.mailgun.net'),
    ],

    'postmark' => [
        'token' => env('POSTMARK_TOKEN'),
    ],

    'ses' => [
        'key' => env('AWS_ACCESS_KEY_ID'),
        'secret' => env('AWS_SECRET_ACCESS_KEY'),
        'region' => env('AWS_DEFAULT_REGION', 'us-east-1'),
    ],

    'korta' => [
        'env' => env('KORTA_ENV_TEST', true),
        'terminal' => env('KORTA_TERMINAL', '50719'),
        'merchant' => env('KORTA_MERCHANT', '8190094'),
        'secret' => env('KORTA_SECRET', '6ADcgKHhfeG4fBvD4r37A2cjLSrn2aFVBiVFR5MX'),

        'api_endpoint' => env('KORTA_API_ENDPOINT', 'https://test.kortathjonustan.is:8443/rpc/'),
        'user' => env('KORTA_USER', 'fit'),
        'pwd' => env('KORTA_PWD', 'test'),
        'api_terminal' => env('KORTA_API_TERMINAL', '8180001'),
        'api_merchant' => env('KORTA_API_MERCHANT', '90000001'),
    ],

    'pei' => [
        'local' => [
            'auth_url' => env('PEI_SANDBOX_AUTH_URL', 'https://authstaging.pei.is/core/connect/token/'),
            'order_url' => env('PEI_SANDBOX_ORDER_URL', 'https://externalapistaging.pei.is/api/orders/'),
            'payment_url' => env('PEI_SANDBOX_PAYMENT_URL', 'https://gattinstaging.pei.is/'),
            'client_id' => env('PEI_SANDBOX_CLIENT_ID', 'democlient'),
            'secret_key' => env('PEI_SANDBOX_SECRET_KEY', 'demosecret'),
            'merchant_id' => env('PEI_SANDBOX_MERCHANT_ID', 1),
        ],
        'production' => [
            'auth_url' => env('PEI_LIVE_AUTH_URL', 'https://auth.pei.is/core/connect/token/'),
            'order_url' => env('PEI_LIVE_ORDER_URL', 'https://api.pei.is/api/orders/'),
            'payment_url' => env('PEI_LIVE_PAYMENT_URL', 'https://gattin.pei.is/'),
            'client_id' => env('PEI_LIVE_CLIENT_ID'),
            'secret_key' => env('PEI_LIVE_SECRET_KEY'),
            'merchant_id' => env('PEI_LIVE_MERCHANT_ID'),
        ],
    ],

    'netgiro' => [
        'app_id' => env('NETGIRO_APP_ID', '881E674F-7891-4C20-AFD8-56FE2624C4B5'),
        'secret_key' => env('NETGIRO_SECRET_KEY', 'YCFd6hiA8lUjZejVcIf/LhRXO4wTDxY0JhOXvQZwnMSiNynSxmNIMjMf1HHwdV6cMN48NX3ZipA9q9hLPb9C1ZIzMH5dvELPAHceiu7LbZzmIAGeOf/OUaDrk2Zq2dbGacIAzU6yyk4KmOXRaSLi8KW8t3krdQSX7Ecm8Qunc/A='),
        'url' => env('NETGIRO_URL', 'https://test.netgiro.is/api/payment/'),
    ],

    'sending' => [
        'api_url' => env('SENDING_API_URL', 'https://api.sending.is/v1/'),
        'api_key' => env('SENDING_API_KEY', 'mJz3BAbaSm9fLmDl2a08Y2JLNg3pF9pb'),
    ],
];
