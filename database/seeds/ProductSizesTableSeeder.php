<?php

use App\Models\ProductSize;
use Illuminate\Database\Seeder;

class ProductSizesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        ProductSize::query()->create([
            'product_id' => 1,
            'target_id' => 1,
            'kcal' => 444,
            'dish' => '{"en": "Skrifstofupakki", "is": "IS Skrifstofupakki"}',
            'sauce' => '{"en": "Skrifstofupakki", "is": "IS Skrifstofupakki"}',
        ]);

        ProductSize::query()->create([
            'product_id' => 2,
            'target_id' => 2,
            'kcal' => 444,
            'dish' => '{"en": "Skrifstofupakki", "is": "IS Skrifstofupakki"}',
            'sauce' => '{"en": "Skrifstofupakki", "is": "IS Skrifstofupakki"}',
        ]);

        ProductSize::query()->create([
            'product_id' => 3,
            'target_id' => 3,
            'kcal' => 444,
            'dish' => '{"en": "Skrifstofupakki", "is": "IS Skrifstofupakki"}',
            'sauce' => '{"en": "Skrifstofupakki", "is": "IS Skrifstofupakki"}',
        ]);
    }
}
