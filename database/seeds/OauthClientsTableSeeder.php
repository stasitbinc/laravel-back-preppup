<?php

use Illuminate\Database\Seeder;

class OauthClientsTableSeeder extends Seeder
{
    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        \DB::table('oauth_clients')->delete();

        \DB::table('oauth_clients')->insert([
            0 => [
                    'created_at' => '2020-03-12 08:50:41',
                    'id' => 1,
                    'name' => 'Laravel Password Grant Client',
                    'password_client' => 1,
                    'personal_access_client' => 0,
                    'redirect' => 'http://localhost',
                    'revoked' => 0,
                    'secret' => 'KlK6C3AZdpVNZxEBsSpYcCCgUZ6ZZ9b7XD0CayFR',
                    'updated_at' => '2020-03-12 08:50:41',
                    'user_id' => null,
                ],
        ]);
    }
}
