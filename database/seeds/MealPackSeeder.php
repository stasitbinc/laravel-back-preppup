<?php

use App\Models\MealPack;
use  Illuminate\Database\Seeder;

class MealPackSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        MealPack::query()->create([
            'meal_pack_count_days_id' => 1,
            'meal_pack_groups_id' => 1,
            'name' => ['en' => 'Well prepped', 'is' => 'Vel preppaður'],
            'slug' => 'well-prepped',
            'sub_title' => ['en' => '5 days of food all day', 'is' => '5 dagar af mat allan daginn'],
            'ingredients' => '',
            'allergy' => '',
            'short_description' => ['en' => 'A package designed for those who want
                to eat healthy and good food for 5 days and cut down and eat smaller
                portions at a time.',
                'is' => 'Pakki hugsaður fyrir þá sem
                vilja borða hollan og góðan mat í 5 daga og skera niður og borða
                minni skammta í einu.', ],
            'description' => ['en' => 'The cut-down package is intended for those who want to cut
                down and eat smaller portions at a time. You can choose how many days you want to
                take at once or simply go all in and buy a subscription.',
                'is' => 'Skera
                niður pakkinn er ætlaður þeim sem vilja skera niður og borða minni skammta í einu. Þú
                getur valið um hvað marga daga þú vilt taka í einu eða einfaldlega farið all in og
                keypt áskrift.', ],
            'price_meal' => '15000',
            'price' => '15000',
            'kcal' => '2500',
            'per_type' => '0',
            'is_public' => '1',
        ]);

        MealPack::query()->create([
            'meal_pack_count_days_id' => 2,
            'meal_pack_groups_id' => 1,
            'name' => ['en' => 'Well prepped', 'is' => 'Vel preppaður'],
            'slug' => 'well-prepped',
            'sub_title' => ['en' => '7 days of food all day', 'is' => '7 dagar af mat allan daginn'],
            'ingredients' => '',
            'allergy' => '',
            'short_description' => ['en' => 'A package designed for those who want
                to eat healthy and good food for 5 days and cut down and eat smaller
                portions at a time.',
                'is' => 'Pakki hugsaður fyrir þá sem
                vilja borða hollan og góðan mat í 5 daga og skera niður og borða
                minni skammta í einu.', ],
            'description' => ['en' => 'The cut-down package is intended for those who want to cut
                down and eat smaller portions at a time. You can choose how many days you want to
                take at once or simply go all in and buy a subscription.',
                'is' => 'Skera
                niður pakkinn er ætlaður þeim sem vilja skera niður og borða minni skammta í einu. Þú
                getur valið um hvað marga daga þú vilt taka í einu eða einfaldlega farið all in og
                keypt áskrift.', ],
            'price_meal' => '20000',
            'price' => '20000',
            'kcal' => '3500',
            'per_type' => '0',
            'is_public' => '1',
        ]);

        MealPack::query()->create([
            'meal_pack_count_days_id' => 3,
            'meal_pack_groups_id' => 1,
            'name' => ['en' => 'Well prepped', 'is' => 'Vel preppaður'],
            'slug' => 'well-prepped',
            'sub_title' => ['en' => '28 days of food all day', 'is' => '28 dagar af mat allan daginn'],
            'ingredients' => '',
            'allergy' => '',
            'short_description' => ['en' => 'A package designed for those who want
                to eat healthy and good food for 5 days and cut down and eat smaller
                portions at a time.',
                'is' => 'Pakki hugsaður fyrir þá sem
                vilja borða hollan og góðan mat í 5 daga og skera niður og borða
                minni skammta í einu.', ],
            'description' => ['en' => 'The cut-down package is intended for those who want to cut
                down and eat smaller portions at a time. You can choose how many days you want to
                take at once or simply go all in and buy a subscription.',
                'is' => 'Skera
                niður pakkinn er ætlaður þeim sem vilja skera niður og borða minni skammta í einu. Þú
                getur valið um hvað marga daga þú vilt taka í einu eða einfaldlega farið all in og
                keypt áskrift.', ],
            'price_meal' => '25000',
            'price' => '25000',
            'kcal' => '5000',
            'per_type' => '0',
            'is_public' => '1',
        ]);

        MealPack::query()->create([
            'meal_pack_count_days_id' => 2,
            'meal_pack_groups_id' => 3,
            'name' => ['en' => 'Well prepped', 'is' => 'Vel preppaður'],
            'slug' => 'well-prepped',
            'sub_title' => ['en' => '5 days of food all day', 'is' => '5 dagar af mat allan daginn'],
            'ingredients' => '',
            'allergy' => '',
            'short_description' => ['en' => 'A package designed for those who want
                to eat healthy and good food for 5 days and cut down and eat smaller
                portions at a time.',
                'is' => 'Pakki hugsaður fyrir þá sem
                vilja borða hollan og góðan mat í 5 daga og skera niður og borða
                minni skammta í einu.', ],
            'description' => ['en' => 'The cut-down package is intended for those who want to cut
                down and eat smaller portions at a time. You can choose how many days you want to
                take at once or simply go all in and buy a subscription.',
                'is' => 'Skera
                niður pakkinn er ætlaður þeim sem vilja skera niður og borða minni skammta í einu. Þú
                getur valið um hvað marga daga þú vilt taka í einu eða einfaldlega farið all in og
                keypt áskrift.', ],
            'price_meal' => '15000',
            'price' => '15000',
            'kcal' => '2500',
            'per_type' => '0',
            'is_public' => '1',
        ]);

        MealPack::query()->create([
            'meal_pack_count_days_id' => 2,
            'meal_pack_groups_id' => 3,
            'name' => ['en' => 'Well prepped', 'is' => 'Vel preppaður'],
            'slug' => 'well-prepped',
            'sub_title' => ['en' => '7 days of food all day', 'is' => '7 dagar af mat allan daginn'],
            'ingredients' => '',
            'allergy' => '',
            'short_description' => ['en' => 'A package designed for those who want
                to eat healthy and good food for 5 days and cut down and eat smaller
                portions at a time.',
                'is' => 'Pakki hugsaður fyrir þá sem
                vilja borða hollan og góðan mat í 5 daga og skera niður og borða
                minni skammta í einu.', ],
            'description' => ['en' => 'The cut-down package is intended for those who want to cut
                down and eat smaller portions at a time. You can choose how many days you want to
                take at once or simply go all in and buy a subscription.',
                'is' => 'Skera
                niður pakkinn er ætlaður þeim sem vilja skera niður og borða minni skammta í einu. Þú
                getur valið um hvað marga daga þú vilt taka í einu eða einfaldlega farið all in og
                keypt áskrift.', ],
            'price_meal' => '20000',
            'price' => '20000',
            'kcal' => '3500',
            'per_type' => '0',
            'is_public' => '1',
        ]);

        MealPack::query()->create([
            'meal_pack_count_days_id' => 3,
            'meal_pack_groups_id' => 3,
            'name' => ['en' => 'Well prepped', 'is' => 'Vel preppaður'],
            'slug' => 'well-prepped',
            'sub_title' => ['en' => '28 days of food all day', 'is' => '28 dagar af mat allan daginn'],
            'ingredients' => '',
            'allergy' => '',
            'short_description' => ['en' => 'A package designed for those who want
                to eat healthy and good food for 5 days and cut down and eat smaller
                portions at a time.',
                'is' => 'Pakki hugsaður fyrir þá sem
                vilja borða hollan og góðan mat í 5 daga og skera niður og borða
                minni skammta í einu.', ],
            'description' => ['en' => 'The cut-down package is intended for those who want to cut
                down and eat smaller portions at a time. You can choose how many days you want to
                take at once or simply go all in and buy a subscription.',
                'is' => 'Skera
                niður pakkinn er ætlaður þeim sem vilja skera niður og borða minni skammta í einu. Þú
                getur valið um hvað marga daga þú vilt taka í einu eða einfaldlega farið all in og
                keypt áskrift.', ],
            'price_meal' => '25000',
            'price' => '25000',
            'kcal' => '5000',
            'per_type' => '0',
            'is_public' => '1',
        ]);

        MealPack::query()->create([
            'meal_pack_count_days_id' => 1,
            'meal_pack_groups_id' => 2,
            'name' =>  ['en' => 'Vegan pakki', 'is' => 'IS Vegan pakki'],
            'slug' => 'well-prepped',
            'sub_title' => ['en' => '5 days of food all day', 'is' => '5 dagar af mat allan daginn'],
            'ingredients' => '',
            'allergy' => '',
            'short_description' => ['en' => 'A package designed for those who want
                to eat healthy and good food for 5 days and cut down and eat smaller
                portions at a time.',
                'is' => 'Pakki hugsaður fyrir þá sem
                vilja borða hollan og góðan mat í 5 daga og skera niður og borða
                minni skammta í einu.', ],
            'description' => ['en' => 'The cut-down package is intended for those who want to cut
                down and eat smaller portions at a time. You can choose how many days you want to
                take at once or simply go all in and buy a subscription.',
                'is' => 'Skera
                niður pakkinn er ætlaður þeim sem vilja skera niður og borða minni skammta í einu. Þú
                getur valið um hvað marga daga þú vilt taka í einu eða einfaldlega farið all in og
                keypt áskrift.', ],
            'price_meal' => '15000',
            'price' => '15000',
            'kcal' => '2500',
            'per_type' => '0',
            'is_public' => '1',
        ]);

        MealPack::query()->create([
            'meal_pack_count_days_id' => 2,
            'meal_pack_groups_id' => 2,
            'name' => ['en' => 'Vegan pakki', 'is' => 'IS Vegan pakki'],
            'slug' => 'well-prepped',
            'sub_title' => ['en' => '7 days of food all day', 'is' => '7 dagar af mat allan daginn'],
            'ingredients' => '',
            'allergy' => '',
            'short_description' => ['en' => 'A package designed for those who want
                to eat healthy and good food for 5 days and cut down and eat smaller
                portions at a time.',
                'is' => 'Pakki hugsaður fyrir þá sem
                vilja borða hollan og góðan mat í 5 daga og skera niður og borða
                minni skammta í einu.', ],
            'description' => ['en' => 'The cut-down package is intended for those who want to cut
                down and eat smaller portions at a time. You can choose how many days you want to
                take at once or simply go all in and buy a subscription.',
                'is' => 'Skera
                niður pakkinn er ætlaður þeim sem vilja skera niður og borða minni skammta í einu. Þú
                getur valið um hvað marga daga þú vilt taka í einu eða einfaldlega farið all in og
                keypt áskrift.', ],
            'price_meal' => '20000',
            'price' => '20000',
            'kcal' => '3500',
            'per_type' => '0',
            'is_public' => '1',
        ]);

        MealPack::query()->create([
            'meal_pack_count_days_id' => 3,
            'meal_pack_groups_id' => 2,
            'name' => ['en' => 'Vegan pakki', 'is' => 'IS Vegan pakki'],
            'slug' => 'well-prepped',
            'sub_title' => ['en' => '28 days of food all day', 'is' => '28 dagar af mat allan daginn'],
            'ingredients' => '',
            'allergy' => '',
            'short_description' => ['en' => 'A package designed for those who want
                to eat healthy and good food for 5 days and cut down and eat smaller
                portions at a time.',
                'is' => 'Pakki hugsaður fyrir þá sem
                vilja borða hollan og góðan mat í 5 daga og skera niður og borða
                minni skammta í einu.', ],
            'description' => ['en' => 'The cut-down package is intended for those who want to cut
                down and eat smaller portions at a time. You can choose how many days you want to
                take at once or simply go all in and buy a subscription.',
                'is' => 'Skera
                niður pakkinn er ætlaður þeim sem vilja skera niður og borða minni skammta í einu. Þú
                getur valið um hvað marga daga þú vilt taka í einu eða einfaldlega farið all in og
                keypt áskrift.', ],
            'price_meal' => '25000',
            'price' => '25000',
            'kcal' => '5000',
            'per_type' => '0',
            'is_public' => '1',
        ]);

        MealPack::query()->create([
            'meal_pack_count_days_id' => 1,
            'meal_pack_groups_id' => 4,
            'name' => ['en' => 'Skrifstofupakki', 'is' => 'IS Skrifstofupakki'],
            'slug' => 'well-prepped',
            'sub_title' =>['en' => '7 days of food all day', 'is' => '7 dagar af mat allan daginn'],
            'ingredients' => '',
            'allergy' => '',
            'short_description' => ['en' => 'A package designed for those who want
                to eat healthy and good food for 5 days and cut down and eat smaller
                portions at a time.',
                'is' => 'Pakki hugsaður fyrir þá sem
                vilja borða hollan og góðan mat í 5 daga og skera niður og borða
                minni skammta í einu.', ],
            'description' => ['en' => 'The cut-down package is intended for those who want to cut
                down and eat smaller portions at a time. You can choose how many days you want to
                take at once or simply go all in and buy a subscription.',
                'is' => 'Skera
                niður pakkinn er ætlaður þeim sem vilja skera niður og borða minni skammta í einu. Þú
                getur valið um hvað marga daga þú vilt taka í einu eða einfaldlega farið all in og
                keypt áskrift.', ],
            'price_meal' => '20000',
            'price' => '20000',
            'kcal' => '3500',
            'per_type' => '0',
            'is_public' => '1',
        ]);

        MealPack::query()->create([
            'meal_pack_count_days_id' => 2,
            'meal_pack_groups_id' => 4,
            'name' => ['en' => 'Skrifstofupakki', 'is' => 'IS Skrifstofupakki'],
            'slug' => 'well-prepped',
            'sub_title' => ['en' => '28 days of food all day', 'is' => '28 dagar af mat allan daginn'],
            'ingredients' => '',
            'allergy' => '',
            'short_description' => ['en' => 'A package designed for those who want
                to eat healthy and good food for 5 days and cut down and eat smaller
                portions at a time.',
                'is' => 'Pakki hugsaður fyrir þá sem
                vilja borða hollan og góðan mat í 5 daga og skera niður og borða
                minni skammta í einu.', ],
            'description' => ['en' => 'The cut-down package is intended for those who want to cut
                down and eat smaller portions at a time. You can choose how many days you want to
                take at once or simply go all in and buy a subscription.',
                'is' => 'Skera
                niður pakkinn er ætlaður þeim sem vilja skera niður og borða minni skammta í einu. Þú
                getur valið um hvað marga daga þú vilt taka í einu eða einfaldlega farið all in og
                keypt áskrift.', ],
            'price_meal' => '25000',
            'price' => '25000',
            'kcal' => '5000',
            'per_type' => '0',
            'is_public' => '1',
        ]);
    }
}
