<?php

use App\Models\Postcode;
use Illuminate\Database\Seeder;

class PostcodesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $postcodes = collect([
            [
                'zip' => 101,
                'standard' => 950,
                'extra' => 950,
            ],
            [
                'zip' => 103,
                'standard' => 950,
                'extra' => 950,
            ],
            [
                'zip' => 104,
                'standard' => 950,
                'extra' => 950,
            ],
            [
                'zip' => 105,
                'standard' => 950,
                'extra' => 950,
            ],
            [
                'zip' => 107,
                'standard' => 950,
                'extra' => 950,
            ],
            [
                'zip' => 108,
                'standard' => 950,
                'extra' => 950,
            ],
            [
                'zip' => 109,
                'standard' => 950,
                'extra' => 950,
            ],
            [
                'zip' => 110,
                'standard' => 950,
                'extra' => 950,
            ],
            [
                'zip' => 111,
                'standard' => 950,
                'extra' => 950,
            ],
            [
                'zip' => 112,
                'standard' => 950,
                'extra' => 950,
            ],
            [
                'zip' => 113,
                'standard' => 950,
                'extra' => 950,
            ],
            [
                'zip' => 116,
                'standard' => 1500,
                'extra' => 1500,
            ],
            [
                'zip' => 170,
                'standard' => 950,
                'extra' => 950,
            ],
            [
                'zip' => 190,
                'standard' => 1500,
                'extra' => 1500,
            ],
            [
                'zip' => 200,
                'standard' => 950,
                'extra' => 950,
            ],
            [
                'zip' => 201,
                'standard' => 950,
                'extra' => 950,
            ],
            [
                'zip' => 202,
                'standard' => 950,
                'extra' => 950,
            ],
            [
                'zip' => 203,
                'standard' => 950,
                'extra' => 950,
            ],
            [
                'zip' => 210,
                'standard' => 950,
                'extra' => 950,
            ],
            [
                'zip' => 220,
                'standard' => 950,
                'extra' => 950,
            ],
            [
                'zip' => 221,
                'standard' => 950,
                'extra' => 950,
            ],
            [
                'zip' => 225,
                'standard' => 950,
                'extra' => 950,
            ],
            [
                'zip' => 230,
                'standard' => 1500,
                'extra' => 1500,
            ],
            [
                'zip' => 233,
                'standard' => 1500,
                'extra' => 1500,
            ],
            [
                'zip' => 235,
                'standard' => 1500,
                'extra' => 1500,
            ],
            [
                'zip' => 240,
                'standard' => 1500,
                'extra' => 1500,
            ],
            [
                'zip' => 245,
                'standard' => 1500,
                'extra' => 1500,
            ],
            [
                'zip' => 250,
                'standard' => 1500,
                'extra' => 1500,
            ],
            [
                'zip' => 260,
                'standard' => 1500,
                'extra' => 1500,
            ],
            [
                'zip' => 270,
                'standard' => 950,
                'extra' => 950,
            ],
            [
                'zip' => 271,
                'standard' => 950,
                'extra' => 950,
            ],
            [
                'zip' => 276,
                'standard' => 950,
                'extra' => 950,
            ],
            [
                'zip' => 800,
                'standard' => 1500,
                'extra' => 1500,
            ],
            [
                'zip' => 801,
                'standard' => 1500,
                'extra' => 1500,
            ],
            [
                'zip' => 802,
                'standard' => 1500,
                'extra' => 1500,
            ],
            [
                'zip' => 810,
                'standard' => 1500,
                'extra' => 1500,
            ],
            [
                'zip' => 815,
                'standard' => 1500,
                'extra' => 1500,
            ],
            [
                'zip' => 816,
                'standard' => 1500,
                'extra' => 1500,
            ],
            [
                'zip' => 820,
                'standard' => 1500,
                'extra' => 1500,
            ],
            [
                'zip' => 825,
                'standard' => 1500,
                'extra' => 1500,
            ],
            [
                'zip' => 850,
                'standard' => 1500,
                'extra' => 1500,
            ],
            [
                'zip' => 851,
                'standard' => 1500,
                'extra' => 1500,
            ],
            [
                'zip' => 860,
                'standard' => 1500,
                'extra' => 1500,
            ],
            [
                'zip' => 861,
                'standard' => 1500,
                'extra' => 1500,
            ],
        ]);

        $postcodes->map(function (array $postcode) {
            PostCode::query()->create($postcode);
        });
    }
}
