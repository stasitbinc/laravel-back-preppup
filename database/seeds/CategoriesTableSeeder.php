<?php

use App\Models\Category;
use Illuminate\Database\Seeder;

class CategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $targets = collect([
            [
                'name' => 'Kjöt/fiskur',
            ],
            [
                'name' => 'Kolvetni',
            ],
            [
                'name' => 'Sósur og marinering',
            ],
            [
                'name' => 'Mjólkarvara',
            ],
            [
                'name' => 'Grænmeti/Ávextir',
            ],
            [
                'name' => 'Krydd',
            ],
            [
                'name' => 'Stk vara',
            ],
            [
                'name' => 'Þurrvara',
            ],
            [
                'name' => 'Frystivara',
            ],
        ]);

        $targets->map(function (array $target) {
            factory(Category::class)->create($target);
        });
    }
}
