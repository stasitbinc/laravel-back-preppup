<?php

use App\Models\User;
use Illuminate\Database\Seeder;
use Treestoneit\ShoppingCart\Facades\Cart;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        /** @var User $user */
        $user = factory(User::class)->create([
            'email' => 'test@test.com',
        ]);

        $user->assignRole('Admin');

        $user = factory(User::class)->create([
            'email' => 'user@user.com',
        ]);

        $user->assignRole('User');
    }
}
