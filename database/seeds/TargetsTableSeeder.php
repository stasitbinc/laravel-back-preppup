<?php

use App\Models\Target;
use Illuminate\Database\Seeder;

class TargetsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $targets = collect([
            [
                'name' => 'Skera niður',
            ],
            [
                'name' => 'Viðhalda',
            ],
            [
                'name' => 'Stækka',
            ],
        ]);

        $targets->map(function (array $target) {
            factory(Target::class)->create($target);
        });
    }
}
