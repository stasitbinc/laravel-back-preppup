<?php

use App\Models\MealPackGroups;
use  Illuminate\Database\Seeder;

class MealPackGroupsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        MealPackGroups::query()->create([
            'title' => ['en' => 'Matur alian daginn', 'is' => 'IS Matur alian daginn'],
            'description' => '',
            'slug' => 'matur',
        ]);

        MealPackGroups::query()->create([
            'title' => ['en' => 'Vegan pakki', 'is' => 'IS Vegan pakki'],
            'description' => '',
            'slug' => 'vegan',
        ]);

        MealPackGroups::query()->create([
            'title' => ['en' => 'Namsmannapakki', 'is' => 'IS Namsmannapakki'],
            'description' => '',
            'slug' => 'namsmannapakki',
        ]);

        MealPackGroups::query()->create([
            'title' => ['en' => 'Skrifstofupakki', 'is' => 'IS Skrifstofupakki'],
            'description' => '',
            'slug' => 'skrifstofupakki',
        ]);
    }
}
