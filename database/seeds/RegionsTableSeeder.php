<?php

use App\Models\Region;
use Illuminate\Database\Seeder;

class RegionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $regions = [
            [
                'name' => 'Höfuðborgarsvæðið',
                'slug' => 'capital',
            ],
            [
                'name' => 'Suðurnes',
                'slug' => 'section_two',
            ],
            [
                'name' => 'Akranes/Borgarnes',
                'slug' => 'section_three',
            ],
            [
                'name' => 'Suðurland',
                'slug' => 'section_four',
            ],
        ];

        collect($regions)->map(function (array $region) {
            Region::query()->create($region);
        });
    }
}
