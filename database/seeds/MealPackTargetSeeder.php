<?php

use App\Models\Pivot\MealPackTarget;
use Illuminate\Database\Seeder;

class MealPackTargetSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        MealPackTarget::query()->create([
            'target_id' => 1,
            'meal_pack_id' => 1,
            'nutritional' => '{"Orka": "1800", "Fita": "94", "Kolvetni" : "94", "Protein" : "1,0"}',
        ]);

        MealPackTarget::query()->create([
            'target_id' => 2,
            'meal_pack_id' => 1,
            'nutritional' => '{"Orka": "800", "Fita": "4", "Kolvetni" : "4", "Protein" : "2,0"}',
        ]);

        MealPackTarget::query()->create([
            'target_id' => 3,
            'meal_pack_id' => 1,
            'nutritional' => '{"Orka": "2800", "Fita": "46", "Kolvetni" : "44", "Protein" : "1,0"}',
        ]);

        MealPackTarget::query()->create([
            'target_id' => 1,
            'meal_pack_id' => 2,
            'nutritional' => '{"Orka": "1800", "Fita": "94", "Kolvetni" : "94", "Protein" : "1,0"}',
        ]);

        MealPackTarget::query()->create([
            'target_id' => 2,
            'meal_pack_id' => 2,
            'nutritional' => '{"Orka": "1800", "Fita": "94", "Kolvetni" : "94", "Protein" : "1,0"}',
        ]);

        MealPackTarget::query()->create([
            'target_id' => 3,
            'meal_pack_id' => 2,
            'nutritional' => '{"Orka": "1700", "Fita": "24", "Kolvetni" : "64", "Protein" : "1,0"}',
        ]);

        MealPackTarget::query()->create([
            'target_id' => 1,
            'meal_pack_id' => 3,
            'nutritional' => '{"Orka": "800", "Fita": "94", "Kolvetni" : "94", "Protein" : "1,0"}',
        ]);

        MealPackTarget::query()->create([
            'target_id' => 2,
            'meal_pack_id' => 3,
            'nutritional' => '{"Orka": "1800", "Fita": "194", "Kolvetni" : "494", "Protein" : "1,0"}',
        ]);

        MealPackTarget::query()->create([
            'target_id' => 3,
            'meal_pack_id' => 3,
            'nutritional' => '{"Orka": "2800", "Fita": "294", "Kolvetni" : "194", "Protein" : "1,0"}',
        ]);

        MealPackTarget::query()->create([
            'target_id' => 1,
            'meal_pack_id' => 4,
            'nutritional' => '{"Orka": "1800", "Fita": "94", "Kolvetni" : "94", "Protein" : "1,0"}',
        ]);

        MealPackTarget::query()->create([
            'target_id' => 2,
            'meal_pack_id' => 5,
            'nutritional' => '{"Orka": "1800", "Fita": "94", "Kolvetni" : "94", "Protein" : "1,0"}',
        ]);

        MealPackTarget::query()->create([
            'target_id' => 2,
            'meal_pack_id' => 6,
            'nutritional' => '{"Orka": "1800", "Fita": "94", "Kolvetni" : "94", "Protein" : "1,0"}',
        ]);

        MealPackTarget::query()->create([
            'target_id' => 3,
            'meal_pack_id' => 7,
            'nutritional' => '{"Orka": "1800", "Fita": "94", "Kolvetni" : "94", "Protein" : "1,0"}',
        ]);

        MealPackTarget::query()->create([
            'target_id' => 3,
            'meal_pack_id' => 8,
            'nutritional' => '{"Orka": "1800", "Fita": "94", "Kolvetni" : "94", "Protein" : "1,0"}',
        ]);

        MealPackTarget::query()->create([
            'target_id' => 3,
            'meal_pack_id' => 9,
            'nutritional' => '{"Orka": "1800", "Fita": "94", "Kolvetni" : "94", "Protein" : "1,0"}',
        ]);
    }
}
