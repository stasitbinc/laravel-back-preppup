<?php

use App\Models\ProductType;
use Illuminate\Database\Seeder;

class ProductTypesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $types = collect([
            [
                'title' => [
                    'is' => 'Vegan',
                    'en' => 'Vegan',
                ],
            ],
            [
                'title' => [
                    'is' => 'Grænmeti',
                    'en' => 'Vegetarian',
                ],
            ],
            [
                'title' => [
                    'is' => 'Kjöt',
                    'en' => 'Meat',
                ],
            ],
            [
                'title' => [
                    'is' => 'Lágkolvetna',
                    'en' => 'Low-Carbs',
                ],
            ],
            [
                'title' => [
                    'is' => 'Millimál',
                    'en' => 'Snack',
                ],
            ],
            [
                'title' => [
                    'is' => 'Barnaréttir',
                    'en' => "Children's meals",
                ],
            ],
        ]);

        $types->map(function ($item) {
            ProductType::query()->create([
                'title' => [
                    'en' => $item['title']['en'],
                    'is' => $item['title']['is'],
                ],
            ]);
        });
    }
}
