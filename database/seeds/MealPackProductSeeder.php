<?php

use App\Models\MealPackProduct;
use Illuminate\Database\Seeder;

class MealPackProductSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        MealPackProduct::query()->create([
            'meal_pack_id' => 1,
            'product_id' => 1,
            'target_id' => 1,
            'week' => 'first',
            'day' => 'one_time',
            'count' => '2',
            'is_hidden' => 0,
            'sending' => 1,
            'diet' => 'breakfast',
        ]);

        MealPackProduct::query()->create([
            'meal_pack_id' => 1,
            'product_id' => 2,
            'target_id' => 1,
            'week' => 'first',
            'day' => 'one_time',
            'count' => '2',
            'is_hidden' => 0,
            'sending' => 1,
            'diet' => 'dinner',
        ]);

        MealPackProduct::query()->create([
            'meal_pack_id' => 1,
            'product_id' => 3,
            'target_id' => 1,
            'week' => 'first',
            'day' => 'one_time',
            'count' => '2',
            'is_hidden' => 0,
            'sending' => 1,
            'diet' => 'lunch',
        ]);

        MealPackProduct::query()->create([
            'meal_pack_id' => 1,
            'product_id' => 4,
            'target_id' => 1,
            'week' => 'first',
            'day' => 'one_time',
            'count' => '2',
            'is_hidden' => 0,
            'sending' => 1,
            'diet' => 'lunch',
        ]);

        MealPackProduct::query()->create([
            'meal_pack_id' => 2,
            'product_id' => 1,
            'target_id' => 1,
            'week' => 'first',
            'day' => 'one_time',
            'count' => '2',
            'is_hidden' => 0,
            'sending' => 1,
            'diet' => 'breakfast',
        ]);

        MealPackProduct::query()->create([
            'meal_pack_id' => 2,
            'product_id' => 2,
            'target_id' => 1,
            'week' => 'first',
            'day' => 'one_time',
            'count' => '2',
            'is_hidden' => 0,
            'sending' => 1,
            'diet' => 'dinner',
        ]);

        MealPackProduct::query()->create([
            'meal_pack_id' => 2,
            'product_id' => 3,
            'target_id' => 1,
            'week' => 'first',
            'day' => 'one_time',
            'count' => '2',
            'is_hidden' => 0,
            'sending' => 1,
            'diet' => 'lunch',
        ]);

        MealPackProduct::query()->create([
            'meal_pack_id' => 2,
            'product_id' => 4,
            'target_id' => 1,
            'week' => 'first',
            'day' => 'one_time',
            'count' => '2',
            'is_hidden' => 0,
            'sending' => 1,
            'diet' => 'lunch',
        ]);

        MealPackProduct::query()->create([
            'meal_pack_id' => 3,
            'product_id' => 1,
            'target_id' => 1,
            'week' => 'first',
            'day' => 'one_time',
            'count' => '2',
            'is_hidden' => 0,
            'sending' => 1,
            'diet' => 'breakfast',
        ]);

        MealPackProduct::query()->create([
            'meal_pack_id' => 3,
            'product_id' => 2,
            'target_id' => 1,
            'week' => 'first',
            'day' => 'one_time',
            'count' => '2',
            'is_hidden' => 0,
            'sending' => 1,
            'diet' => 'dinner',
        ]);

        MealPackProduct::query()->create([
            'meal_pack_id' => 3,
            'product_id' => 3,
            'target_id' => 1,
            'week' => 'first',
            'day' => 'one_time',
            'count' => '2',
            'is_hidden' => 0,
            'sending' => 1,
            'diet' => 'lunch',
        ]);

        MealPackProduct::query()->create([
            'meal_pack_id' => 3,
            'product_id' => 4,
            'target_id' => 1,
            'week' => 'first',
            'day' => 'one_time',
            'count' => '2',
            'is_hidden' => 0,
            'sending' => 1,
            'diet' => 'lunch',
        ]);

        MealPackProduct::query()->create([
            'meal_pack_id' => 4,
            'product_id' => 1,
            'target_id' => 1,
            'week' => 'first',
            'day' => 'one_time',
            'count' => '2',
            'is_hidden' => 0,
            'sending' => 1,
            'diet' => 'breakfast',
        ]);

        MealPackProduct::query()->create([
            'meal_pack_id' => 4,
            'product_id' => 2,
            'target_id' => 1,
            'week' => 'first',
            'day' => 'one_time',
            'count' => '2',
            'is_hidden' => 0,
            'sending' => 1,
            'diet' => 'dinner',
        ]);

        MealPackProduct::query()->create([
            'meal_pack_id' => 4,
            'product_id' => 3,
            'target_id' => 1,
            'week' => 'first',
            'day' => 'one_time',
            'count' => '2',
            'is_hidden' => 0,
            'sending' => 1,
            'diet' => 'lunch',
        ]);

        MealPackProduct::query()->create([
            'meal_pack_id' => 4,
            'product_id' => 4,
            'target_id' => 1,
            'week' => 'first',
            'day' => 'one_time',
            'count' => '2',
            'is_hidden' => 0,
            'sending' => 1,
            'diet' => 'lunch',
        ]);

        MealPackProduct::query()->create([
            'meal_pack_id' => 5,
            'product_id' => 1,
            'target_id' => 2,
            'week' => 'first',
            'day' => 'one_time',
            'count' => '2',
            'is_hidden' => 0,
            'sending' => 1,
            'diet' => 'breakfast',
        ]);

        MealPackProduct::query()->create([
            'meal_pack_id' => 5,
            'product_id' => 2,
            'target_id' => 2,
            'week' => 'first',
            'day' => 'one_time',
            'count' => '2',
            'is_hidden' => 0,
            'sending' => 1,
            'diet' => 'dinner',
        ]);

        MealPackProduct::query()->create([
            'meal_pack_id' => 5,
            'product_id' => 3,
            'target_id' => 2,
            'week' => 'first',
            'day' => 'one_time',
            'count' => '2',
            'is_hidden' => 0,
            'sending' => 1,
            'diet' => 'lunch',
        ]);

        MealPackProduct::query()->create([
            'meal_pack_id' => 5,
            'product_id' => 4,
            'target_id' => 2,
            'week' => 'first',
            'day' => 'one_time',
            'count' => '2',
            'is_hidden' => 0,
            'sending' => 1,
            'diet' => 'lunch',
        ]);

        MealPackProduct::query()->create([
            'meal_pack_id' => 6,
            'product_id' => 1,
            'target_id' => 2,
            'week' => 'first',
            'day' => 'one_time',
            'count' => '2',
            'is_hidden' => 0,
            'sending' => 1,
            'diet' => 'breakfast',
        ]);

        MealPackProduct::query()->create([
            'meal_pack_id' => 6,
            'product_id' => 2,
            'target_id' => 2,
            'week' => 'first',
            'day' => 'one_time',
            'count' => '2',
            'is_hidden' => 0,
            'sending' => 1,
            'diet' => 'dinner',
        ]);

        MealPackProduct::query()->create([
            'meal_pack_id' => 6,
            'product_id' => 3,
            'target_id' => 2,
            'week' => 'first',
            'day' => 'one_time',
            'count' => '2',
            'is_hidden' => 0,
            'sending' => 1,
            'diet' => 'lunch',
        ]);

        MealPackProduct::query()->create([
            'meal_pack_id' => 6,
            'product_id' => 4,
            'target_id' => 2,
            'week' => 'first',
            'day' => 'one_time',
            'count' => '2',
            'is_hidden' => 0,
            'sending' => 1,
            'diet' => 'lunch',
        ]);

        MealPackProduct::query()->create([
            'meal_pack_id' => 7,
            'product_id' => 1,
            'target_id' => 3,
            'week' => 'first',
            'day' => 'one_time',
            'count' => '2',
            'is_hidden' => 0,
            'sending' => 1,
            'diet' => 'breakfast',
        ]);

        MealPackProduct::query()->create([
            'meal_pack_id' => 7,
            'product_id' => 2,
            'target_id' => 3,
            'week' => 'first',
            'day' => 'one_time',
            'count' => '2',
            'is_hidden' => 0,
            'sending' => 1,
            'diet' => 'dinner',
        ]);

        MealPackProduct::query()->create([
            'meal_pack_id' => 7,
            'product_id' => 3,
            'target_id' => 3,
            'week' => 'first',
            'day' => 'one_time',
            'count' => '2',
            'is_hidden' => 0,
            'sending' => 1,
            'diet' => 'lunch',
        ]);

        MealPackProduct::query()->create([
            'meal_pack_id' => 7,
            'product_id' => 4,
            'target_id' => 3,
            'week' => 'first',
            'day' => 'one_time',
            'count' => '2',
            'is_hidden' => 0,
            'sending' => 1,
            'diet' => 'lunch',
        ]);

        MealPackProduct::query()->create([
            'meal_pack_id' => 8,
            'product_id' => 1,
            'target_id' => 3,
            'week' => 'first',
            'day' => 'one_time',
            'count' => '2',
            'is_hidden' => 0,
            'sending' => 1,
            'diet' => 'breakfast',
        ]);

        MealPackProduct::query()->create([
            'meal_pack_id' => 8,
            'product_id' => 2,
            'target_id' => 3,
            'week' => 'first',
            'day' => 'one_time',
            'count' => '2',
            'is_hidden' => 0,
            'sending' => 1,
            'diet' => 'dinner',
        ]);

        MealPackProduct::query()->create([
            'meal_pack_id' => 8,
            'product_id' => 3,
            'target_id' => 3,
            'week' => 'first',
            'day' => 'one_time',
            'count' => '2',
            'is_hidden' => 0,
            'sending' => 1,
            'diet' => 'lunch',
        ]);

        MealPackProduct::query()->create([
            'meal_pack_id' => 8,
            'product_id' => 4,
            'target_id' => 3,
            'week' => 'first',
            'day' => 'one_time',
            'count' => '2',
            'is_hidden' => 0,
            'sending' => 1,
            'diet' => 'lunch',
        ]);

        MealPackProduct::query()->create([
            'meal_pack_id' => 9,
            'product_id' => 1,
            'target_id' => 3,
            'week' => 'first',
            'day' => 'one_time',
            'count' => '2',
            'is_hidden' => 0,
            'sending' => 1,
            'diet' => 'breakfast',
        ]);

        MealPackProduct::query()->create([
            'meal_pack_id' => 9,
            'product_id' => 2,
            'target_id' => 3,
            'week' => 'first',
            'day' => 'one_time',
            'count' => '2',
            'is_hidden' => 0,
            'sending' => 1,
            'diet' => 'dinner',
        ]);

        MealPackProduct::query()->create([
            'meal_pack_id' => 9,
            'product_id' => 3,
            'target_id' => 3,
            'week' => 'first',
            'day' => 'one_time',
            'count' => '2',
            'is_hidden' => 0,
            'sending' => 1,
            'diet' => 'lunch',
        ]);

        MealPackProduct::query()->create([
            'meal_pack_id' => 9,
            'product_id' => 4,
            'target_id' => 3,
            'week' => 'first',
            'day' => 'one_time',
            'count' => '2',
            'is_hidden' => 0,
            'sending' => 1,
            'diet' => 'lunch',
        ]);
    }
}
