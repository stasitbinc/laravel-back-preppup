<?php

use App\Models\Allergy;
use Illuminate\Database\Seeder;

class AllergiesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $types = collect([
            [
                'title' => [
                    'is' => 'Gluten',
                    'en' => 'Gluten',
                ],
            ],
            [
                'title' => [
                    'is' => 'Sykur',
                    'en' => 'Sugar',
                ],
            ],
            [
                'title' => [
                    'is' => 'Skelfiskur',
                    'en' => 'Shellfish',
                ],
            ],
            [
                'title' => [
                    'is' => 'Laktósi',
                    'en' => 'Lactose',
                ],
            ],
            [
                'title' => [
                    'is' => 'Hnetur',
                    'en' => 'Nuts',
                ],
            ],
            [
                'title' => [
                    'is' => 'Egg',
                    'en' => 'Eggs',
                ],
            ],
        ]);

        $types->map(function ($item) {
            Allergy::query()->create([
                'title' => [
                    'en' => $item['title']['en'],
                    'is' => $item['title']['is'],
                ],
            ]);
        });
    }
}
