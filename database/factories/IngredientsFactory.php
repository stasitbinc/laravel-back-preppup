<?php

/** @var Factory $factory */

use App\Models\Category;
use App\Models\Ingredient;
use Faker\Generator as Faker;
use Illuminate\Database\Eloquent\Factory;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(Ingredient::class, function (Faker $faker) {
    $faker->addProvider(new \FakerRestaurant\Provider\en_US\Restaurant($faker));

    return [
        'name' => $faker->foodName(),
        'calories' => $faker->randomNumber(2),
        'fat' => $faker->randomFloat(1, 0, 10),
        'protein' => $faker->randomFloat(1, 0, 10),
        'carbohydrates' => $faker->randomFloat(1, 0, 10),
        'category_id' => Category::query()->inRandomOrder()->first(),
    ];
});
