<?php

/** @var Factory $factory */

use App\Enums\DayEnum;
use App\Enums\WeekEnum;
use App\Models\MealPack;
use App\Models\Product;
use App\Models\Target;
use Faker\Generator as Faker;
use Illuminate\Database\Eloquent\Factory;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(MealPack::class, function (Faker $faker) {
    return [
        'name' => ['en' => $faker->words(2, true), 'is' => $faker->words(2, true)],
        'sub_title' => ['en' => $faker->words(2, true), 'is' => $faker->words(2, true)],
        'description' => ['en' => $faker->realText(200), 'is' => $faker->realText(200)],
        'short_description' => ['en' => $faker->realText(50), 'is' => $faker->realText(50)],
        'ingredients' => ['en' => $faker->realText(200), 'is' => $faker->realText(200)],
        'allergy' => ['en' => $faker->realText(200), 'is' => $faker->realText(200)],
        'target_id' => Target::query()->inRandomOrder()->first(),
        'price' => $faker->randomNumber(4),
        'price_meal' => $faker->randomNumber(3),
        'kcal' => $faker->randomNumber(4),
        'is_public' => $faker->boolean,
        'per_type' => $faker->boolean,
        'nutritional' => [
            'energy' => $faker->randomNumber(2),
            'fat' => $faker->randomFloat(1, 0, 10),
            'protein' => $faker->randomFloat(1, 0, 10),
            'carbohydrates' => $faker->randomFloat(1, 0, 10),
        ],
    ];
});

$factory->afterCreating(MealPack::class, function (MealPack $mealPack, Faker $faker) {
    $products = Product::all()->random(rand(1, 10));

    $keyed = $products->mapWithKeys(function (Product $product) use ($faker) {
        return [$product->getKey() => ['day' => $faker->randomElement(DayEnum::values()), 'week' => $faker->randomElement(WeekEnum::values()), 'target_id' => Target::query()->inRandomOrder()->first()->getKey()]];
    });

    $mealPack->products()->sync($keyed->toArray());

    $rand = rand(1, 5);

    $mealPack->addMediaFromUrl(asset("img/packs/$rand.png"))->toMediaCollection('pack');
});
