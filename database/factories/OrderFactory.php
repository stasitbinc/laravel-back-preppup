<?php

/** @var Factory $factory */

use App\Enums\DeliveryEnum;
use App\Enums\OrderStatusEnum;
use App\Enums\PaymentTypeEnum;
use App\Enums\ReservationOrderEnum;
use App\Models\Allergy;
use App\Models\Coupon;
use App\Models\Order;
use App\Models\Postcode;
use App\Models\User;
use Faker\Generator as Faker;
use Illuminate\Database\Eloquent\Factory;
use Illuminate\Support\Str;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(Order::class, function (Faker $faker) {
    return [
        'user_id' => User::query()->inRandomOrder()->first(),
        'coupon_id' => $faker->boolean(50) ? Coupon::query()->inRandomOrder()->value('id') : null,
        'overview' => [
            'done' => $faker->boolean(50),
            'reservations' => $faker->randomElement(ReservationOrderEnum::values()),
            'pay_difference' => $faker->boolean(50),
        ],
        'allergy' => [
            'done' => $faker->boolean(50),
            'enabled' => $faker->boolean(50),
            'allergies' => Allergy::query()->inRandomOrder()->limit($faker->numberBetween(1, Allergy::query()->count()))->get()->pluck('id'),
            'intolerance' => $faker->realText(50, 2),
        ],
        'delivery' => [
            'done' => $faker->boolean(50),
            'if' => $faker->realText(50, 2),
            'date' => $faker->date('Y-m-d'),
            'type' => DeliveryEnum::DAY,
        ],
        'address' => [
            'done' => $faker->boolean(50),
            'is_same' => $faker->boolean(50),
            'city' => $faker->city,
            'floor' => $faker->secondaryAddress,
            'address' => $faker->streetAddress,
            'postcode_id' => Postcode::query()->inRandomOrder()->first()->getKey(),
        ],
        'payment' => [
            'done' => $faker->boolean(50),
            'type' => PaymentTypeEnum::KORTA,
            'korta' => [
                'card4' => $faker->randomNumber(4),
                'cardbrand' => $faker->creditCardType,
                'time' => $faker->date('d-m-y H:i:s'),
                'token' => strtolower(Str::random(19)),
                'authcode' => $faker->ean8,
                'reference' => $faker->isbn10,
                'downloadmd5' => $faker->md5,
                'transaction' => $faker->ean8,
            ],
        ],
        'step' => $faker->randomElement(['overview', 'allergy', 'delivery', 'address', 'payment']),
        'status' => $faker->randomElement(OrderStatusEnum::values()),
        'date' => $faker->dateTimeBetween('-1 years', 'now'),
        'amount' => [
            'total' => $faker->randomNumber(4),
            'coupon' => $faker->randomNumber(3),
            'shipping' => Postcode::query()->inRandomOrder()->first()->getAttribute('standard'),
            'difference' => $faker->randomNumber(3),
            'for_payment' => $faker->randomNumber(4),
        ],
        'created_at' => $faker->dateTimeBetween('-2 years', 'now'),
        'updated_at' => $faker->dateTimeBetween('-2 years', 'now'),
    ];
});
