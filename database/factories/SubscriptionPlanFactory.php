<?php

/** @var Factory $factory */

use App\Models\SubscriptionPlan;
use Faker\Generator as Faker;
use Illuminate\Database\Eloquent\Factory;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(SubscriptionPlan::class, function (Faker $faker) {
    return [
        'days' => $faker->randomNumber(2),
        'one_meal' => $faker->randomNumber(4),
        'two_meal' => $faker->randomNumber(4),
        'three_meal' => $faker->randomNumber(4),
        'hours_to_save' => $faker->randomNumber(1),
    ];
});
