<?php

/** @var Factory $factory */

use App\Models\Cart;
use App\Models\CartItem;
use Faker\Generator as Faker;
use Illuminate\Database\Eloquent\Factory;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(CartItem::class, function (Faker $faker) {
    return [
        'cart_id' => Cart::query()->inRandomOrder()->first(),
        'quantity' => $faker->randomDigitNotNull,
        'buyable_type' => $faker->randomElement(['Product', 'MealPack']),
        'buyable_id' => $faker->numberBetween(1, 20),
        'kcal' => $faker->randomNumber(4),
    ];
});
