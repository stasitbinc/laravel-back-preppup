<?php

/** @var Factory $factory */

use App\Models\Faq;
use App\Models\FaqCategory;
use Faker\Generator as Faker;
use Illuminate\Database\Eloquent\Factory;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(Faq::class, function (Faker $faker) {
    return [
        'question' => ['en' => $faker->words(2, true), 'is' => $faker->words(2, true)],
        'answer' => ['en' => $faker->realText(50), 'is' => $faker->realText(50)],
        'faq_category_id' => FaqCategory::query()->inRandomOrder()->first(),
    ];
});
