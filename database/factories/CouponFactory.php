<?php

/** @var Factory $factory */

use App\Models\Coupon;
use Faker\Generator as Faker;
use Illuminate\Database\Eloquent\Factory;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(Coupon::class, function (Faker $faker) {
    return [
        'title' => $faker->word,
        'code' => $faker->numerify("$faker->word ###"),
        'meal_packs' => $faker->randomNumber(2),
        'products' => $faker->randomNumber(2),
        'is_enabled' => $faker->boolean,
    ];
});
