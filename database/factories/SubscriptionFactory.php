<?php

/** @var Factory $factory */

use App\Enums\SubscriptionStatusEnum;
use App\Models\Buyable;
use App\Models\Order;
use App\Models\Subscription;
use App\Models\User;
use Faker\Generator as Faker;
use Illuminate\Database\Eloquent\Factory;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(Subscription::class, function (Faker $faker) {
    return [
        'user_id' => User::query()->inRandomOrder()->first(),
        'buyable_id' => Buyable::query()->inRandomOrder()->first(),
        'order_id' => Order::query()->inRandomOrder()->first(),
        'status' => $faker->randomElement(SubscriptionStatusEnum::values()),
    ];
});
