<?php

/** @var Factory $factory */

use App\Models\Buyable;
use App\Models\Order;
use Faker\Generator as Faker;
use Illuminate\Database\Eloquent\Factory;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(Buyable::class, function (Faker $faker) {
    return [
        'order_id' => Order::query()->inRandomOrder()->first(),
        'quantity' => $faker->randomDigitNotNull,
        'buyable_type' => $faker->randomElement(['Product', 'MealPack']),
        'buyable_id' => $faker->numberBetween(1, 20),
        'kcal' => $faker->randomNumber(4),
        'price' => $faker->randomNumber(4),
    ];
});
