<?php

/** @var Factory $factory */

use App\Models\Product;
use App\Models\ProductSize;
use App\Models\ProductType;
use Faker\Generator as Faker;
use Illuminate\Database\Eloquent\Factory;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(Product::class, function (Faker $faker) {
    return [
        'name' => ['en' => $faker->words(3, true), 'is' => $faker->words(3, true)],
        'ingredients' => ['en' => $faker->realText(200), 'is' => $faker->realText(200)],
        'allergy' => ['en' => $faker->realText(200), 'is' => $faker->realText(200)],
        'product_type_id' => ProductType::query()->inRandomOrder()->first(),
        'price' => $faker->randomNumber(4),
    ];
});

$factory->afterCreating(Product::class, function (Product $product, $faker) {
    $rand = rand(1, 2);

//    $sizes = ProductSize::all()->random($rand)
//        ->mapWithKeys(function (ProductSize $productSize) use ($faker) {
//            return [$productSize->getKey() => [
//                'dish' => [
//                    'energy' => $faker->randomNumber(2),
//                    'fat' => $faker->randomFloat(1, 0, 10),
//                    'protein' => $faker->randomFloat(1, 0, 10),
//                    'carbohydrates' => $faker->randomFloat(1, 0, 10),
//                ],
//                'sauce' => [
//                    'energy' => $faker->randomNumber(2),
//                    'fat' => $faker->randomFloat(1, 0, 10),
//                    'protein' => $faker->randomFloat(1, 0, 10),
//                    'carbohydrates' => $faker->randomFloat(1, 0, 10),
//                ],
//            ]];
//        })->toArray();

//    $product->product_sizes()->syncWithoutDetaching($sizes);

    $product->addMediaFromUrl(asset("img/products/$rand.png"))->toMediaCollection('product');

    for ($i = 1; $i <= $rand; $i++) {
        $product->addMediaFromUrl(asset("img/products/$i.png"))->toMediaCollection('product_gallery');
    }
});
