<?php

/** @var Factory $factory */

use App\Models\Post;
use App\Models\Target;
use Faker\Generator as Faker;
use Illuminate\Database\Eloquent\Factory;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(Post::class, function (Faker $faker) {
    return [
        'title' => ['en' => $faker->words(2, true), 'is' => $faker->words(2, true)],
        'description' => ['en' => $faker->realText(200), 'is' => $faker->realText(200)],
        'name' => ['en' => $faker->name, 'is' => $faker->name],
        'target_id' => Target::query()->inRandomOrder()->first(),
    ];
});

$factory->afterCreating(Post::class, function (Post $post, $faker) {
    $rand = rand(1, 3);

    $post->addMediaFromUrl(asset("img/posts/$rand.png"))->toMediaCollection('post');
});
