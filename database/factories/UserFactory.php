<?php

/** @var Factory $factory */

use App\Enums\GenderEnum;
use App\Models\Postcode;
use App\Models\User;
use Faker\Generator as Faker;
use Illuminate\Database\Eloquent\Factory;
use Illuminate\Support\Str;
use JoeDixon\Translation\Language;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(User::class, function (Faker $faker) {
    return [
        'name' => $faker->name,
        'age' => $faker->randomNumber(),
        'gender' => $faker->randomElement(GenderEnum::values()),
        'email' => $faker->unique()->safeEmail,
        'phone' => $faker->phoneNumber,
        'address' => $faker->streetAddress,
        'floor' => $faker->secondaryAddress,
        'postcode_id' => Postcode::query()->inRandomOrder()->first(),
        'language_id' => Language::query()->inRandomOrder()->first(),
        'city' => $faker->city,
        'email_verified_at' => now(),
        'password' => bcrypt('secret'),
        'remember_token' => Str::random(10),
    ];
});
