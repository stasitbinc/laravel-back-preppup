<?php

/** @var Factory $factory */

use App\Models\Cart;
use App\Models\User;
use Faker\Generator as Faker;
use Illuminate\Database\Eloquent\Factory;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(Cart::class, function (Faker $faker) {
    return [
        'user_id' => $faker->unique()->randomElement(User::query()->pluck('id')->toArray()),
    ];
});
