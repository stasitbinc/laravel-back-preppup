<?php

use App\Models\Post;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumnTargetIdToPostsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Post::query()->delete();

        Schema::table('posts', function (Blueprint $table) {
            $table->unsignedBigInteger('target_id')->nullable()->after('name');

            $table->foreign('target_id')->references('id')->on('targets')->onDelete('cascade');

            $table->dropForeign('posts_meal_pack_type_id_foreign');

            $table->dropColumn(['meal_pack_type_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('posts', function (Blueprint $table) {
            $table->dropForeign('posts_target_id_foreign');

            $table->dropColumn(['target_id']);

            $table->unsignedBigInteger('meal_pack_type_id')->after('name');

            $table->foreign('meal_pack_type_id')->references('id')->on('meal_pack_types')->onDelete('cascade');
        });
    }
}
