<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumnTargetIdToMealPackProductTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('meal_pack_product', function (Blueprint $table) {
            $table->unsignedBigInteger('target_id')->nullable()->after('product_id');

            $table->foreign('target_id')->references('id')->on('targets')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('meal_pack_product', function (Blueprint $table) {
            $table->dropForeign('meal_pack_product_target_id_foreign');

            $table->dropColumn(['target_id']);
        });
    }
}
