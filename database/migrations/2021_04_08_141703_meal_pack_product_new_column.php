<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class MealPackProductNewColumn extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('meal_pack_product', function (Blueprint $table) {
            $table->string('sending')->nullable();
            $table->string('diet')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('meal_pack_product', function (Blueprint $table) {
            $table->dropColumn(['sending', 'diet']);
        });
    }
}
