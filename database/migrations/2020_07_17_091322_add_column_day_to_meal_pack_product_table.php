<?php

use App\Enums\DayEnum;
use App\Models\Pivot\MealPackProduct;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumnDayToMealPackProductTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('meal_pack_product', function (Blueprint $table) {
            $table->string('day')->nullable()->after('week');
        });

        MealPackProduct::all()->map(function (MealPackProduct $mealPackProduct) {
            $mealPackProduct->update([
                'day' => DayEnum::MONDAY,
            ]);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('meal_pack_product', function (Blueprint $table) {
            $table->dropColumn(['day']);
        });
    }
}
