<?php

use App\Models\MealPack;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumnIsPublicToMealPacksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('meal_packs', function (Blueprint $table) {
            $table->boolean('is_public')->default(false)->after('per_type');
        });

        MealPack::all()->map(function (MealPack $mealPack) {
            $mealPack->update([
                'is_public' => true,
            ]);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('meal_packs', function (Blueprint $table) {
            $table->dropColumn(['is_public']);
        });
    }
}
