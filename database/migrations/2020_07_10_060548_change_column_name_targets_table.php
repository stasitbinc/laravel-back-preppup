<?php

use App\Models\Target;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ChangeColumnNameTargetsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $targets = Target::all();

        $targets->map(function (Target $target) {
            $target->update([
               'name' => null,
            ]);
        });

        Schema::table('targets', function (Blueprint $table) {
            $table->json('name')->change();
        });

        $targets->map(function (Target $target) {
            $enName = 'Cut down';
            $name = 'Skera niður';

            if ($target->getKey() == 2) {
                $enName = 'Maintenance';
                $name = 'Viðhalda';
            }

            if ($target->getKey() == 3) {
                $enName = 'Bulk';
                $name = 'Stækka';
            }

            if ($target->getKey() == 4) {
                $enName = 'Vegan';
                $name = 'Vegan';
            }

            $target->update([
               'name' => [
                   'is' => $name,
                   'en' => $enName,
               ],
            ]);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('targets', function (Blueprint $table) {
            $table->string('name')->change();
        });
    }
}
