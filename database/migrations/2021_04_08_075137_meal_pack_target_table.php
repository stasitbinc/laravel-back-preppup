<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class MealPackTargetTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('meal_pack_target', function (Blueprint $table) {
            $table->unsignedBigInteger('target_id');
            $table->unsignedBigInteger('meal_pack_id');

            $table->foreign('target_id')->references('id')->on('targets')->onDelete('cascade');
            $table->foreign('meal_pack_id')->references('id')->on('meal_packs')->onDelete('cascade');
            $table->json('nutritional')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('meal_pack_target');
    }
}
