<?php

use App\Models\Postcode;
use App\Models\Region;
use Illuminate\Database\Migrations\Migration;

class AttachRegionsToPostcodes extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $capital = [101, 102, 103, 104, 105, 107, 108, 109, 110, 111, 112, 113, 116, 121, 123, 124, 125, 127, 128, 129, 130, 132, 150, 155, 170, 172, 200, 201, 202, 203, 210, 212, 220, 221, 222, 225, 210, 270, 271, 276];

        $regionTwo = [190, 230, 232, 233, 235, 240, 245, 250, 260];

        $regionThree = [300, 301, 302, 310, 311];

        $regionFour = [800, 801, 802, 810, 815, 816, 820, 825, 850, 851, 860, 861];

        $postcodes = Postcode::all();

        $postcodes->map(function (Postcode $postcode) use ($capital, $regionTwo, $regionThree, $regionFour) {
            $code = $postcode->getAttribute('zip');

            if (in_array($code, $capital)) {
                $postcode->update([
                    'region_id' => Region::query()->where('slug', 'capital')->first()->getKey(),
                ]);
            }

            if (in_array($code, $regionTwo)) {
                $postcode->update([
                    'region_id' => Region::query()->where('slug', 'section_two')->first()->getKey(),
                ]);
            }

            if (in_array($code, $regionThree)) {
                $postcode->update([
                    'region_id' => Region::query()->where('slug', 'section_three')->first()->getKey(),
                ]);
            }

            if (in_array($code, $regionFour)) {
                $postcode->update([
                    'region_id' => Region::query()->where('slug', 'section_four')->first()->getKey(),
                ]);
            }
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
    }
}
