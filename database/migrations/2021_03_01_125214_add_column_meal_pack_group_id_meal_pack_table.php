<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumnMealPackGroupIdMealPackTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('meal_packs', function (Blueprint $table) {
            $table->unsignedBigInteger('meal_pack_groups_id')->nullable()->after('id');

            $table->foreign('meal_pack_groups_id')->references('id')->on('meal_pack_groups')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('meal_packs', function (Blueprint $table) {
            $table->dropForeign('meal_packs_meal_pack_groups_id_foreign');
            $table->dropColumn(['meal_pack_groups_id']);
        });
    }
}
