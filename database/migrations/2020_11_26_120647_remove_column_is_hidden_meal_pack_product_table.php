<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class RemoveColumnIsHiddenMealPackProductTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('meal_pack_product', function (Blueprint $table) {
            $table->dropColumn(['is_hidden']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('meal_pack_product', function (Blueprint $table) {
            $table->boolean('is_hidden')->default(false)->after('day');
        });
    }
}
