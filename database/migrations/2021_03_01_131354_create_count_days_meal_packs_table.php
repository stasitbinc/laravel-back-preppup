<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCountDaysMealPacksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('count_days_meal_packs', function (Blueprint $table) {
            $table->unsignedBigInteger('meal_packs_id')->nullable();
            $table->unsignedBigInteger('count_days_id')->nullable();

            $table->foreign('meal_packs_id')->references('id')->on('meal_packs')->onDelete('cascade');
            $table->foreign('count_days_id')->references('id')->on('count_days')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('count_days_meal_packs');
    }
}
