<?php

use App\Models\Pivot\MealPackProduct;
use Illuminate\Database\Migrations\Migration;

class ConnectMealPackProductWithTargets extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $mealPackProducts = MealPackProduct::all();

        $mealPackProducts->map(function (MealPackProduct $mealPackProduct) {
            $mealPack = $mealPackProduct->meal_pack()->first();

            $mealPackProduct->update([
                'target_id' => $mealPack->getAttribute('target_id'),
            ]);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
