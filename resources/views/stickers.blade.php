<!doctype html>
<html lang="is">
<head>
    <title>PDF Create</title>

    <style type="text/css">
        .page-break {
            page-break-after: always;
        }
    </style>
</head>
<body>

@foreach($products as $product)
    <div style="transform-origin: left; transform: translate(100%, 50%) rotate(90deg)">

        <div style="width: 100%; height: 40px; margin-bottom: 50px;">
            <h1 style="float: right">{{$product->getTranslation('name', 'is')}}</h1><br>
        </div>
        <div style="float: left; width: 30%; height: 40%; padding-right: 50px; padding-left: 50px; border-right: 2px solid black;">
            Fjarlægið lokið og hitið í örbylgjuofni í 1-2 mínútur. Geymist I allt að 5 daga í kæli. Geymist í frysti ef
            um lengri geymslutíma sé aö ræöa.<br>
            <br>
            Ofnæmislavaldar: <b style="text-transform: uppercase;">
                {{implode(', ', explode('<br>', strip_tags($product->getTranslation('allergy', 'is'), '<br>')))}}
            </b>

            <div style="font-size: 25px; margin-top: 20px; width: 100%; white-space: nowrap">
                <b>P.D. 17/12/20</b>
            </div>
        </div>

        <div style="float: left; width: 55%; height: 50%; margin-left: 50px;">


            <div style="width: 100%; position: relative;">
                <div style="position: absolute; left: -40px; top: 50px; transform: rotate(-90deg)">
                    <b>Réttur</b>
                </div>
                <div style="display:inline-block; width: 23%; text-align: center;">
                    <h4 style="text-align: center;">Kaloríur</h4>
                    <div style="height: 40px; width: 40px; margin-left: 8px; border-radius: 20px 20px 20px 20px; border: 3px solid black;">
                        <div style="hyphens: auto; margin-top: 10px; text-align: center;">
                            {{round($product->sizes[$product->target->getTranslation('name', 'en')]['calories'])}}
                        </div>
                    </div>
                </div>
                <div style="display:inline-block; width: 23%; text-align: center;">
                    <h4>Fita</h4>
                    <div style="height: 40px; width: 40px; border-radius: 20px; border: 3px solid black; margin-left: 8px;">
                        <div style="hyphens: auto; margin-top: 10px; text-align: center;">
                            {{round($product->sizes[$product->target->getTranslation('name', 'en')]['fat'], 1)}}
                        </div>
                    </div>
                </div>
                <div style="display:inline-block; width: 23%; text-align: center;">
                    <h4>Kolvetni</h4>
                    <div style="height: 40px; width: 40px; border-radius: 20px; border: 3px solid black; margin-left: 8px;">
                        <div style="hyphens: auto; margin-top: 10px; text-align: center;">
                            {{round($product->sizes[$product->target->getTranslation('name', 'en')]['carbohydrates'], 1)}}
                        </div>
                    </div>
                </div>
                <div style="display:inline-block; width: 23%; text-align: center;">
                    <h4>Prótein</h4>
                    <div style="height: 40px; width: 40px; border-radius: 20px; border: 3px solid black; margin-left: 8px;">
                        <div style="hyphens: auto; margin-top: 10px; text-align: center;">
                            {{round($product->sizes[$product->target->getTranslation('name', 'en')]['protein'], 1)}}
                        </div>
                    </div>
                </div>
            </div>


            @if($product->product_ingredients()->whereHas('category', function ($query) { $query->where('name', 'Sósur'); })->exists())

                <hr>

                <div style="width: 100%; position: relative; margin-top: 30px;">
                    <div style="position: absolute; left: -32px; top: 10px; transform: rotate(-90deg)">
                        <b>Sósa</b>
                    </div>
                    <div style="display:inline-block; width: 23%; text-align: center">
                        <div style="height: 40px; width: 40px; border-radius: 20px 20px 20px 20px; border: 3px solid black; margin-left: 8px;">
                            <div style="hyphens: auto; margin-top: 10px; text-align: center;">
                                {{round($product->product_ingredients()->whereHas('category', function ($query) { $query->where('name', 'Sósur'); })->first()->calories)}}
                            </div>
                        </div>
                    </div>
                    <div style="display:inline-block; width: 23%; text-align: center;">
                        <div style="height: 40px; width: 40px; border-radius: 20px; border: 3px solid black; margin-left: 8px;">
                            <div style="hyphens: auto; margin-top: 10px; text-align: center;">
                                {{round($product->product_ingredients()->whereHas('category', function ($query) { $query->where('name', 'Sósur'); })->first()->fat, 1)}}
                            </div>
                        </div>
                    </div>
                    <div style="display:inline-block; width: 23%; text-align: center;">
                        <div style="height: 40px; width: 40px; border-radius: 20px; border: 3px solid black; margin-left: 8px;">
                            <div style="hyphens: auto; margin-top: 10px; text-align: center;">
                                {{round($product->product_ingredients()->whereHas('category', function ($query) { $query->where('name', 'Sósur'); })->first()->carbohydrates, 1)}}
                            </div>
                        </div>
                    </div>
                    <div style="display:inline-block; width: 23%; text-align: center;">
                        <div style="height: 40px; width: 40px; border-radius: 20px; border: 3px solid black; margin-left: 8px;">
                            <div style="hyphens: auto; margin-top: 10px; text-align: center;">
                                {{round($product->product_ingredients()->whereHas('category', function ($query) { $query->where('name', 'Sósur'); })->first()->protein, 1)}}
                            </div>
                        </div>
                    </div>
                </div>

            @else
                <div style="margin-top: 90px; float: left;"></div>
            @endif

            <div style="width: 100%; float: left; margin-top: 70px;">
                <h2 style="text-transform: uppercase;">{{$product->target->getTranslation('name', 'is')}}</h2>
            </div>
        </div>
    </div>
    @if (!$loop->last)
        <div class="page-break"></div>
    @endif

@endforeach
</body>
</html>
