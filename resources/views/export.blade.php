<!doctype html>
<html lang="en">
<head>
    <style>
        * {
            box-sizing: border-box;
            padding: 0;
            margin: 0;
            list-style: none;
            text-decoration: none;
            font-family: 'Gilroy';
            font-weight: 400;
        }

        .section-order {
            padding-bottom: 0;
            background-color: #fff;
        }

        .section-order {
            padding-bottom: 0;
            background-color: #fff;
        }

        .order-step-six .step-wrapper {
            padding-bottom: 10px;
        }

        .step-wrapper {
            padding: 32px 0 33px;
            background-color: #FAFAFA;
            min-height: 412px;
        }

        .inner {
            width: 100%;
            margin: 0 auto;
        }

        .step-wrapper .inner {
            padding: 0px 50px;
        }

        .step-wrapper .step-title {
            font-family: Gilroy;
            font-style: normal;
            font-weight: bold;
            font-size: 32px;
            line-height: 150%;
            color: #0B2A3E;
            margin-bottom: 35px;
        }

        .order-step-six .step-wrapper .step-title {
            margin-bottom: 7px;
        }

        .order-step-six .step-title-number {
            display: block;
            margin-bottom: 10px;
            margin-right: 10px;
        }

        .order-step-six .step-title-p {
            font-family: 'Gilroy';
            font-style: normal;
            font-weight: normal;
            font-size: 24px;
            line-height: 150%;
            color: #0B2A3E;
        }

        .order-step-six .step-title-p span {
            font-weight: 600;
        }

        .step-wrapper .wrapper {
            display: block;
        }

        .step-wrapper .left {
            width: 100%;
        }

        .step-wrapper .step-5 .left {
        }

        .step-wrapper .item {
            display: block;
            width: 100%;
            flex: 0 0 100%;
            max-width: 100%;
            padding-top: 32px;
            margin-top: 32px;
            border-top: 1px solid #CED4D8;
        }

        .step-wrapper .item.mt-0 {
            padding-top: 0;
            margin-top: 0;
            border-top: 0 solid #CED4D8;
        }

        .step-wrapper .item h3 {
            margin-top: 0;
            font-family: Gilroy;
            font-style: normal;
            font-weight: 600;
            font-size: 18px;
            line-height: 150%;
            color: #0B2A3E;
            margin-bottom: 20px;
        }

        .step-wrapper .item.mt-0 h3 {
            margin-bottom: 0;
            padding-top: 20px;
        }

        .step-wrapper .item a {
            font-family: Gilroy;
            font-style: normal;
            font-weight: 600;
            font-size: 18px;
            line-height: 150%;
            color: #0B2A3E;
            padding-right: 34px;
            background-repeat: no-repeat;
            background-position: right center;
        }

        .order-step-six .item a, .order-step-six .payment-wrapper::before {
            display: none;
        }

        .step-product-wrapper {
            width: 100%;
            border-collapse: collapse;
            margin-top: 32px;
        }

        .step-product-wrapper td {
            background: #FFFFFF;
            padding: 20px 24px;
            font-family: Gilroy;
            font-style: normal;
            font-weight: 600;
            font-size: 18px;
            line-height: 150%;
            color: #0B2A3E;
            margin-bottom: 6px;
        }

        .step-product-wrapper td:first-child {
            border-radius: 30px 0 0 30px;
        }

        .step-product-wrapper img {
            max-height: 56px;
            display: block;
        }

        .step-product-wrapper td:nth-child(2) {
            width: 200px;
            flex: 0 0 200px;
        }

        .step-product-wrapper td.price-text {
            font-family: Gilroy;
            font-style: normal;
            font-weight: 600;
            font-size: 18px;
            line-height: 150%;
            color: #9DAAB2;
        }

        .order-step-five .step-product-wrapper td:nth-child(4) {
            display: none !important;
        }

        .text-right {
            text-align: right;
            justify-content: flex-end;
        }

        .step-product-wrapper td:last-child {
            border-radius: 0 30px 30px 0;
        }

        .table-date-pick td:last-child {
            width: 100%;
        }

        .step-wrapper .wrapper {
            display: block;
        }

        .step-wrapper .item {
            display: block;
            width: 100%;
            flex: 0 0 100%;
            max-width: 100%;
            padding-top: 32px;
            margin-top: 32px;
            border-top: 1px solid #CED4D8;
        }

        .step-wrapper .item h3 {
            margin-top: 0;
            font-family: Gilroy;
            font-style: normal;
            font-weight: 600;
            font-size: 18px;
            line-height: 150%;
            color: #0B2A3E;
            margin-bottom: 20px;
        }

        .step-wrapper .item p {
            font-family: Gilroy;
            font-style: normal;
            font-weight: 500;
            font-size: 16px;
            line-height: 150%;
            color: #3C5565;
            margin-bottom: 13px;
            margin-top: 0;
            padding-left: 24px;
        }

        .step-wrapper .item a {
            font-family: Gilroy;
            font-style: normal;
            font-weight: 600;
            font-size: 18px;
            line-height: 150%;
            color: #0B2A3E;
            padding-right: 34px;
            background-repeat: no-repeat;
            background-position: right center;
        }

        .order-step-six .item a, .order-step-six .payment-wrapper::before {
            display: none;
        }

        .step-wrapper .right {
            width: 100%;
            padding-top: 45px;
        }

        .step-wrapper .step-5 .right {
            padding-top: 0;
        }

        .form-wrapper {
            position: relative;
        }

        .step-wrapper .step-5 .form-wrapper {
            padding: 26px;
        }

        .order-step-six .step-wrapper .form-wrapper {
            background: none;
            box-shadow: none;
        }

        .step-wrapper .form-wrapper > div {
            max-width: 100%;
            width: 100%;
        }

        .step-wrapper .form-wrapper h3 {
            font-family: Gilroy;
            font-style: normal;
            font-weight: 600;
            font-size: 18px;
            line-height: 150%;
            color: #0B2A3E;
            margin-bottom: 21px;
        }

        .step-wrapper .price {
            width: 100%;
            margin-top: 40px;
            border-bottom: 1px solid #CED4D8;
            padding-bottom: 9px;
            margin-bottom: 16px;
        }

        .step-wrapper .price td:last-child {
            text-align: right;
        }

        .step-wrapper .step-5 .price tr td {
            font-family: Gilroy;
            font-style: normal;
            font-weight: 500;
            font-size: 16px;
            line-height: 150%;
            color: #0B2A3E;
            padding: 8px 0;
        }

        .step-wrapper .step-5 .price tr:first-child td {
            font-family: Gilroy;
            font-style: normal;
            font-weight: 500;
            font-size: 16px;
            line-height: 150%;
            color: #0B2A3E;
        }

        .step-wrapper .step-5 .price tr:nth-child(2) td:last-child {
            font-family: Gilroy;
            font-style: normal;
            font-weight: 500;
            font-size: 16px;
            line-height: 150%;
            color: #68C159;
        }

        .step-wrapper .total {
            width: 100%;
        }

        .step-wrapper .total td:first-child {
            font-family: Gilroy;
            font-style: normal;
            font-weight: 600;
            font-size: 18px;
            line-height: 150%;
            color: #0B2A3E;
        }

        .step-wrapper .total td:last-child {
            font-family: Gilroy;
            font-style: normal;
            font-weight: 600;
            font-size: 24px;
            line-height: 150%;
            color: #0B2A3E;
            text-align: right;
        }

        .payment-wrapper {
            padding-top: 32px;
            margin-top: 36px;
            position: relative;
        }

        .order-step-six .payment-wrapper {
            padding-top: 0px;
            margin-top: 88px;
        }

        .order-step-six .payment-wrapper {
            padding-left: 2px;
        }

        .step-wrapper .form-wrapper > div {
            max-width: 344px;
            width: 100%;
        }

        .step-wrapper .form-wrapper > div {
            max-width: 100%;
        }

        .step-wrapper .form-wrapper h3 {
            font-family: Gilroy;
            font-style: normal;
            font-weight: 600;
            font-size: 18px;
            line-height: 150%;
            color: #0B2A3E;
            margin-bottom: 21px;
        }

        .payment-item {
            display: block;
            max-width: 344px;
            width: 100%;
        }

        .order-step-five .revers-order, .order-step-five .order-form {
            display: block;
            max-width: 344px;
            width: 100%;
        }

        .payment-item .form-group {
            flex: 0 0 100%;
            max-width: 100%;
            width: 100%;
        }

        .checkbox-wrapper label {
            font-family: Gilroy;
            font-style: normal;
            font-weight: 500;
            font-size: 16px;
            line-height: 150%;
            color: #0B2A3E;
            margin-top: 24px;
            display: block;
            position: relative;
        }

        .checkbox-wrapper input {
            vertical-align: bottom;
        }
    </style>
</head>
<body>
<main class="main">
    <section class="section-order order-step-five order-step-six">
        <div class="step-wrapper">
            <div class="inner">
                <div class="step-5">
                    <div class="step-title-number">
                        <div>
                            <p class="step-title-p">{{__('Your order number is ')}}<span>#{{$order->uuid}}</span></p>
                        </div>
                    </div>
                    <div class="wrapper">
                        <div class="left">
                            <div class="item title-item mt-0">
                                <h3>{{__('Overview')}}</h3>
                            </div>
                            <table class="step-product-wrapper table-date-pick">
                                @foreach($order->buyables as $item)
                                    <tr>
                                        <td><img src="{{$item->buyable->thumb}}" alt="{{$item->buyable->name}}"></td>
                                        <td>{{$item->buyable->name}}</td>
                                        <td>
                                            x{{$item->quantity}}
                                        </td>
                                        <td class="price-text text-right" nowrap></td>
                                        <td class="price-text text-right" nowrap>{{$item->price}} kr.</td>
                                    </tr>
                                @endforeach
                            </table>
                        </div>
                        <div class="right">
                            <div class="form-wrapper">
                                <div>
                                    <h3>{{__('Payment Information')}}</h3>
                                    <table class="price">
                                        @if($order->delivery->type == 'day')
                                            <tr>
                                                <td>{{__('Total')}}</td>
                                                <td>{{$order->amount->total + $order->amount->shipping}} .kr</td>
                                            </tr>
                                        @endif
                                        @if($order->delivery->type == 'days')
                                            <tr>
                                                <td>{{__('Total')}}</td>
                                                <td>{{$order->amount->total}} .kr</td>
                                            </tr>
                                        @endif
                                        @if($order->coupon)
                                            <tr>
                                                <td>{{__('Promo code')}}</td>
                                                <td>{{$order->amount->coupon}} .kr</td>
                                            </tr>
                                        @endif
                                        @if($order->delivery->type == 'days')
                                            <tr>
                                                <td>{{__('Extra delivery')}}</td>
                                                <td>{{$order->amount->shipping}} .kr</td>
                                            </tr>
                                        @endif
                                    </table>
                                    <table class="total">
                                        <tr>
                                            <td>{{__('For payment')}}</td>
                                            <td>{{$order->amount->for_payment}} kr.</td>
                                        </tr>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <div class="wrapper">
                            @if($order->allergy->enabled)
                                <div class="item">
                                    <div>
                                        <h3>{{__('Allergy')}}</h3>
                                        <p>{{__('Allergies')}} - {{$order->allergy->intolerance}}</p>
                                    </div>
                                </div>
                            @endif
                            <div class="item">
                                <div>
                                    <h3>{{__('Delivery')}}</h3>
                                    <p>{{\Carbon\Carbon::parse($order->delivery->date)->format('l, d.F')}}</p>
                                </div>
                            </div>
                            <div class="item">
                                <div>
                                    <h3>{{__('Address')}}</h3>
                                    @if($order->address->is_same)
                                        <p>{{$order->user->address}}
                                            , {{$order->user->postcode->zip}} {{$order->user->city}}</p>
                                        <p>{{$order->user->floor}}</p>
                                    @else
                                        <p>{{$order->address->address}}
                                            , {{$order->address->postcode}} {{$order->address->city}}</p>
                                        <p>{{$order->address->floor}}</p>
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</main>
</body>
</html>
