<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'failed' => 'Upplýsingarnar sem þú skráðir finnast ekki hjá okkur.',
    'throttle' => ' Þú hefur reynt að skrá þig inn of oft. Reyndu aftur eftir :seconds seconds.',
    'invalid_credentials' => [
        'error'   => 'Rangt lykilorð eða netfang',
        'message' => 'Lykilorðið eða netfangið sem þú skráðir er ekki rétt.',
    ],
];
