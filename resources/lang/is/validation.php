<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | following language lines contain default error messages used by
    | validator class. Some of these rules have multiple versions such
    | as size rules. Feel free to tweak each of these messages here.
    |
    */

    'accepted' => ':attribute verður að vera samþykkt.',
    'active_url' => ':attribute er ekki gildur hlekkur.',
    'after' => ':attribute verður að vera dagsetning eftir :date.',
    'after_or_equal' => ':attribute verður að vera dagsetning á eða eftir :date.',
    'alpha' => ':attribute may only contain letters.',
    'alpha_dash' => ':attribute getur aðeins notað stafi, tölur,bandstrik og undirstrikað.',
    'alpha_num' => ':attribute getur aðeins notað stafi eða tölur .',
    'array' => ':Reiturinn :attribute verður að vera fylki',
    'before' => ':attribute verður að vera dagsetning fyrir :date.',
    'before_or_equal' => ' :attribute verður að vera dagsetning á eða fyrir :date.',
    'between' => [
        'numeric' => ':attribute verður að vera á milli :min and :max.',
        'file' => ':attribute verður að vera á milli :min and :max Kb.',
        'string' => ':attribute verður að vera á milli :min and :max stafabila.',
        'array' => ':attribute verður að vera á milli :min and :max vara.',
    ],
    'boolean' => ':Reiturinn :attribute verður að vera réttur eða rangur.',
    'confirmed' => ':attribute passar ekki.',
    'date' => ':attribute er ekki gild dagsetning.',
    'date_equals' => ':attribute  verður að vera á :date.',
    'date_format' => ':attribute er ekki rétt sett fram:format.',
    'different' => ':attribute og :other verður að vera mismunandi.',
    'digits' => ':attribute verður að vera:digits digits.',
    'digits_between' => ':attribute verður að vera á milli :min and :max digits.',
    'dimensions' => ':attribute er ekki í réttri stærð .',
    'distinct' => ':attribute reiturinn endurtekur upplýsingar.',
    'email' => ':attribute verður að innihalda gilt tölvupóstfang.',
    'ends_with' => ':attribute verður að enda með: :values.',
    'exists' => ' :attribute sem þú valdir er ekki gilt.',
    'file' => ':attribute verður að vera skrá.',
    'filled' => ':attribute reiturinnverður að vera fylltur út.',
    'gt' => [
        'numeric' => ':attribute verður að vera meiri en :value.',
        'file' => ':attribute verður að vera meiri en :value Kb.',
        'string' => ':attribute verður að hafa fleiri en :value stafabil.',
        'array' => ':attribute verður að vera fleiri en :value vara.',
    ],
    'gte' => [
        'numeric' => ':attribute verður að vera meiri eða jafnt og :value.',
        'file' => ':attribute verður að vera stærra eða jafnt og :value Kb.',
        'string' => ':attribute verður að vera lengra eða jafnt og :value stafabil.',
        'array' => ':attribute verður að hafa :value vara eða meira.',
    ],
    'image' => ':attribute verður að vera mynd.',
    'in' => 'Valin :attribute er ekki gild.',
    'in_array' => ':attribute reiturin  er ekki til í :other.',
    'integer' => ':attribute verður að vera heil tala.',
    'ip' => ':attribute verður að vera gild ip tala.',
    'ipv4' => ':attribute verður að vera IPv4 tala.',
    'ipv6' => ':attribute verður að vera IPv6 tala .',
    'json' => ':attribute verður að vera JSON strengur.',
    'lt' => [
        'numeric' => ':attribute verður að vera minna en :value.',
        'file' => ':attribute verður að vera minna en :value Kb.',
        'string' => ':attribute verður að vera styttra en:value stafabil.',
        'array' => ':attribute verður að hafa færri en :value vara.',
    ],
    'lte' => [
        'numeric' => ':attribute verður að vera minna eða jafnt við :value.',
        'file' => ':attribute verður að vera minna eða jafnt :value Kb.',
        'string' => ':attribute verður að vera færri eða jafn marga  :value stafabil.',
        'array' => ':attribute má ekki hafa færri en :value vara.',
    ],
    'max' => [
        'numeric' => ':attribute má ekki vera hærri en  :max.',
        'file' => ':attribute  má ekki vera meiri en :max Kb.',
        'string' => ':attribute  má ekki vera fleiri en :max stafabil.',
        'array' => ':attribute  má ekki vera hærri en :max vörur.',
    ],
    'mimes' => ':attribute verður að vera: :values skrá.',
    'mimetypes' => ':attribute verður að vera:values skrá.',
    'min' => [
        'numeric' => ':attribute verður að vera að minnsta kosti :min.',
        'file' => ':attribute verður að vera að minnsta kosti :min Kb.',
        'string' => ':attribute verður að vera að minnsta kosti :min stafabil.',
        'array' => ':attribute verður að vera að minnsta kosti :min vörur.',
    ],
    'not_in' => 'Valin :attribute er ekki gilt .',
    'not_regex' => ':attribute sniðið er ekki gilt .',
    'numeric' => ':attribute verður að vera tala.',
    'password' => 'password er rangt.',
    'present' => ':attribute reiturinn verður að vera til staðar.',
    'regex' => ':attribute  sniði er ekki gilt.',
    'required' => ':attribute reiturinn verður að vera útfylltur.',
    'required_if' => ':attribute verður að vera útfylltur ef :other er :value.',
    'required_unless' => ':attribute verður að vera útfylltur nema ef:other er :values.',
    'required_with' => ':attribute  verður að vera útfylltur ef :values er til staðar.',
    'required_with_all' => ':attribute  verður að vera útfylltur þegar :values er til staðar.',
    'required_without' => ':attribute  verður að vera útfylltur ef að :values er ekki til staðar.',
    'required_without_all' => ':attribute  verður að vera útfylltur engin :values er til staðar.',
    'same' => ':attribute og :other verða að passa.',
    'size' => [
        'numeric' => ':attribute verða að vera :size.',
        'file' => ':attribute verða að vera:size Kb.',
        'string' => ':attribute verða að vera :size stafabil.',
        'array' => ':attribute verða að vera :size vörur.',
    ],
    'starts_with' => ':attribute verður að byrja á : :values.',
    'string' => ':attribute verður að vera strengur.',
    'timezone' => ':attribute verður að vera gilt tímabelti.',
    'unique' => ':attribute er þegar í notkun.',
    'uploaded' => ':attribute var ekki hlaðið upp.',
    'url' => ':Snið attribute er ekki gilt.',
    'uuid' => ':attribute verður að vera gilt UUID.',

    /*
    |--------------------------------------------------------------------------
    | Custom Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | Here you may specify custom validation messages for attributes using the
    | convention "attribute.rule" to name lines. This makes it quick to
    | specify a specific custom language line for a given attribute rule.
    |
    */

    'custom' => [
        'attribute-name' => [
            'rule-name' => 'custom-message',
        ],
    ],

    /*
    |--------------------------------------------------------------------------
    | Custom Validation Attributes
    |--------------------------------------------------------------------------
    |
    | following language lines are used to swap our attribute placeholder
    | with something more reader friendly such as "E-Mail Address" instead
    | of "email". This simply helps us make our message more expressive.
    |
    */

    'attributes' => [],
];
