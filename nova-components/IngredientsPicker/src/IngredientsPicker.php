<?php

namespace Preppup\IngredientsPicker;

use Illuminate\Support\Facades\Log;
use Laravel\Nova\Fields\Field;
use Laravel\Nova\Http\Requests\NovaRequest;

class IngredientsPicker extends Field
{
    /**
     * The field's component.
     *
     * @var string
     */
    public $component = 'ingredients-picker';

    public $showOnIndex = false;

    public function fill(NovaRequest $request, $model)
    {
        $request = $request->merge([
            'product_ingredients' => json_decode($request->get('product_ingredients'))
        ]);

        return parent::fill($request, $model);
    }
}
