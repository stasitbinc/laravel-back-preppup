Nova.booting((Vue, router, store) => {
  Vue.component('index-meal-pack-info', require('./components/IndexField'))
  Vue.component('detail-meal-pack-info', require('./components/DetailField'))
  Vue.component('form-meal-pack-info', require('./components/FormField'))
})
