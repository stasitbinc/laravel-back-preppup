<?php

namespace App\Observers;

use App\Models\Coupon;

class CouponObserver
{
    public function creating(Coupon $coupon)
    {
        $this->updateCoupon($coupon);
    }

    public function updating(Coupon $coupon)
    {
        $this->updateCoupon($coupon);
    }

    public function updateCoupon(Coupon $coupon)
    {
        $products = $coupon->getAttribute('products');

        $mealPacks = $coupon->getAttribute('meal_packs');

        if ($products == 100 || $mealPacks == 100) {
            $coupon->forceFill([
                'products' => 100,
                'meal_packs' => 100,
            ]);
        }
    }
}
