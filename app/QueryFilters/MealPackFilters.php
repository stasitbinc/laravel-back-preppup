<?php

namespace App\QueryFilters;

use Cerbero\QueryFilters\QueryFilters;
use Illuminate\Database\Eloquent\Builder;

class MealPackFilters extends QueryFilters
{
    public function target($target)
    {
        $this->query->whereHas('target', function (Builder $query) use ($target) {
            $query->where('slug', $target);
        });
    }

    public function direction($direction)
    {
        switch ($direction) {
            case 'desc':
                $this->query->orderByDesc('slug');
                break;
            default:
                $this->query->orderBy('slug');
        }
    }
}
