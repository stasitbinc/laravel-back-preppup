<?php

namespace App\Console\Commands;

use App\Enums\OrderStatusEnum;
use App\Jobs\CheckAndExportUsersProducts;
use App\Models\Order;
use Illuminate\Console\Command;

class UsersProductsExportCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'user:products';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        Order::query()
            ->where('status', OrderStatusEnum::PAID)
            ->chunk(100, function ($orders) {
                CheckAndExportUsersProducts::dispatchNow(collect($orders));
            });
    }
}
