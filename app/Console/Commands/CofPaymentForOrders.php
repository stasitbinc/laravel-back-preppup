<?php

namespace App\Console\Commands;

use App\Models\Order;
use App\Models\User;
use App\Payment\Korta\Client;
use App\Services\OrderService;
use Illuminate\Console\Command;
use Illuminate\Support\Arr;

class CofPaymentForOrders extends Command
{
    private $client;

    private $service;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'orders:cof';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @param Client $client
     * @param OrderService $service
     */
    public function __construct(Client $client, OrderService $service)
    {
        $this->client = $client;
        $this->service = $service;

        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle(): void
    {
        $orderIds = [103, 110, 112];

        $orders = Order::query()->whereIn('id', $orderIds)->get();

        $orders->map(function (Order $order) {
            /** @var User $user */
            $user = $order->user()->first();

            $cofResponse = $this->client->cofPayment($order, $order->getAttribute('amount')->total);

            $cofResponseCode = Arr::get($cofResponse, 'd39');

            $request = [
                'time' => $order->getAttribute('payment')->korta->time,
                'downloadmd5' => $order->getAttribute('payment')->korta->downloadmd5,
                'authcode' => $order->getAttribute('payment')->korta->authcode,
                'cardbrand' => $order->getAttribute('payment')->korta->cardbrand,
                'card4' => $order->getAttribute('payment')->korta->card4,
            ];

            if ($cofResponseCode == 000) {
                $this->service->success($order, $request, $user, $cofResponse, false);
            }

            if ($cofResponseCode != 000) {
                $this->service->error($order, $user, $cofResponse, false);
            }
        });
    }
}
