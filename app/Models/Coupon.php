<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Coupon extends Model
{
    protected $fillable = [
        'title',
        'code',
        'meal_packs',
        'products',
        'is_enabled',
        'expiration_date',
        'max_products',
    ];

    protected $casts = [
        'is_enabled' => 'boolean',
        'expiration_date' => 'datetime:Y-m-d',
    ];

    protected $dates = [
        'created_at',
        'deleted_at',
        'expiration_date',
    ];

    public function orders()
    {
        return $this->hasMany(Order::class);
    }
}
