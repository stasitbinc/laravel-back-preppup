<?php

namespace App\Models;

use App\Models\Pivot\MealPackProduct;
use App\Models\Pivot\MealPackTarget;
use App\Traits\HasTranslations;
use Cerbero\QueryFilters\FiltersRecords;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Arr;
use Spatie\MediaLibrary\HasMedia\HasMedia;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;
use Spatie\MediaLibrary\Models\Media;
use Spatie\Sluggable\HasSlug;
use Spatie\Sluggable\SlugOptions;

class MealPack extends Model implements HasMedia
{
    use HasMediaTrait, HasTranslations, HasSlug, FiltersRecords, SoftDeletes;

    public $translatable = [
        'name',
        'sub_title',
        'description',
        'short_description',
        'ingredients',
        'allergy',
    ];

    protected $fillable = [
        'name',
        'sub_title',
        'description',
        'short_description',
        'ingredients',
        'allergy',
        'price',
        'price_meal',
        'kcal',
        'slug',
        'per_type',
        'target_id',
        'meal_pack_groups_id',
        'meal_pack_count_days_id',
        'is_public',
        'weeks',
    ];

    protected $guarded = [
        'id',
        'created_at',
    ];

    protected $casts = [
        'per_type' => 'boolean',
        'is_public' => 'boolean',
        'kcal' => 'integer',
    ];

    protected $dates = [
        'updated_at',
        'created_at',
    ];

    protected $hidden = [
        'media',
    ];

    protected $appends = [
        'image',
        'thumb',
        'sizes',
    ];

    public function getWeeksAttribute($value)
    {
        if ($value) {
            return $value;
        }

        $mealPackProducts = MealPackProduct::query()
            ->groupBy('week')
            ->where('meal_pack_id', $this->getKey())
            ->pluck('week');

        return $mealPackProducts->count();
    }

    public function getSizesAttribute()
    {
        $products = $this->products()->get();

        $targets = Target::all();

        $targets = $targets->mapWithKeys(function (Target $target) {
            return [$target->getAttribute('name') => $target->getKey()];
        });

        $products->map(function (Product $product) use ($targets) {
            $sizes = $product->getAttribute('sizes');

            $sizes->map(function (array $size, $key) use ($targets) {
                $targets[$key] = [
                    'calories' => round(Arr::get($targets[$key], 'calories', 0) + Arr::get($size, 'calories', 0), 2),
                    'fat' => round(Arr::get($targets[$key], 'fat', 0) + Arr::get($size, 'fat', 0), 2),
                    'carbohydrates' => round(Arr::get($targets[$key], 'carbohydrates', 0) + Arr::get($size, 'carbohydrates', 0), 2),
                    'protein' => round(Arr::get($targets[$key], 'protein', 0) + Arr::get($size, 'protein', 0), 2),
                ];
            });
        });

        return $targets->filter(function ($size) {
            return gettype($size) == 'array';
        });
    }

    public function getSlugOptions(): SlugOptions
    {
        return SlugOptions::create()
            ->generateSlugsFrom('name')
            ->saveSlugsTo('slug');
    }

    public function getRouteKeyName()
    {
        return 'slug';
    }

    public function products()
    {
        return $this->belongsToMany(Product::class, 'meal_pack_product')
            ->using(MealPackProduct::class)
            ->withPivot(['week', 'target_id', 'day', 'count', 'sending', 'diet']);
    }

    public function targets()
    {
        return $this->belongsToMany(Target::class, 'meal_pack_target')
            ->using(MealPackTarget::class)
            ->withPivot(['nutritional']);
    }

    public function meal_pack_product()
    {
        return $this->hasMany(MealPackProduct::class);
    }

    public function meal_pack_target()
    {
        return $this->hasMany(MealPackTarget::class);
    }

    public function meal_pack_infos()
    {
        return $this->hasMany(MealPackInfo::class);
    }

    public function meal_pack_groups()
    {
        return $this->belongsTo(MealPackGroups::class);
    }

    public function meal_pack_count_days()
    {
        return $this->belongsTo(MealPackCountDays::class);
    }

    public function orders()
    {
        return $this->morphToMany(Order::class, 'buyable');
    }

    public function registerMediaConversions(Media $media = null)
    {
        $this->addMediaConversion('thumb')
            ->width(366)
            ->keepOriginalImageFormat()
            ->height(366);
    }

    public function registerMediaCollections()
    {
        $this->addMediaCollection('pack')->singleFile();
    }

    public function getThumbAttribute()
    {
        if ($this->hasMedia('pack')) {
            return asset($this->getFirstMedia('pack')->getUrl('thumb'));
        }

        return asset('img/no-image.jpg');
    }

    public function getImageAttribute()
    {
        if ($this->hasMedia('pack')) {
            return asset($this->getFirstMediaUrl('pack'));
        }

        return asset('img/no-image.jpg');
    }

    public function scopeMealPackGroupsId($query, $id)
    {
        return $query->whereHas('meal_pack_groups', function ($query) use ($id) {
            $query->where('id', $id);
        });
    }

    public function scopeMealPackCountDaysSlug($query, $slug)
    {
        return $query->whereHas('meal_pack_count_days', function ($query) use ($slug) {
            $query->where('slug', $slug);
        });
    }
}
