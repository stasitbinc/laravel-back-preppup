<?php

namespace App\Models;

use App\Traits\HasTranslations;
use Illuminate\Database\Eloquent\Model;

class Faq extends Model
{
    use HasTranslations;

    public $translatable = [
        'question',
        'answer',
    ];

    protected $fillable = [
        'question',
        'answer',
        'faq_category_id',
    ];

    protected $guarded = [
        'id',
        'created_at',
    ];

    protected $dates = [
        'updated_at',
        'created_at',
    ];

    public function faq_category()
    {
        return $this->belongsTo(FaqCategory::class);
    }
}
