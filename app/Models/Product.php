<?php

namespace App\Models;

use App\Models\Pivot\MealPackProduct;
use App\Models\Pivot\ProductProductSize;
use App\Models\Pivot\ProductTarget;
use App\Traits\HasCalculatedSize;
use App\Traits\HasTranslations;
use Cerbero\QueryFilters\FiltersRecords;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\MediaLibrary\HasMedia\HasMedia;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;
use Spatie\MediaLibrary\Models\Media;

class Product extends Model implements HasMedia
{
    use HasMediaTrait, HasTranslations, FiltersRecords, SoftDeletes, HasCalculatedSize;

    public $translatable = [
        'name',
        'ingredients',
        'allergy',
    ];

    protected $fillable = [
        'name',
        'ingredients',
        'allergy',
        'product_type_id',
        'price',
        'is_additional',
        'is_hidden',
    ];

    protected $casts = [
        'is_additional' => 'boolean',
        'is_hidden' => 'boolean',
    ];

    protected $guarded = [
        'id',
        'created_at',
        'product_ingredients',
    ];

    protected $dates = [
        'updated_at',
        'created_at',
    ];

    protected $hidden = [
        'media',
    ];

    protected $appends = [
        'image',
        'images',
        'thumb',
        'product_size',
        'sizes',
    ];

    public function product_targets()
    {
        return $this->hasMany(ProductTarget::class);
    }

    public function meal_packs()
    {
        return $this->belongsToMany(MealPack::class, 'meal_pack_product')
            ->using(MealPackProduct::class)
            ->withPivot(['week', 'target_id', 'day', 'count', 'sending', 'diet']);
    }

    public function product_sizes()
    {
        return $this->hasMany(ProductSize::class);
    }

    public function targets()
    {
        return $this->belongsToMany(Target::class, 'product_target')
            ->using(ProductTarget::class)
            ->withPivot(['size', 'category_id', 'ingredient_id']);
    }

    public function product_ingredients()
    {
        return $this->belongsToMany(Ingredient::class, 'product_target')
            ->using(ProductTarget::class)
            ->withPivot(['size', 'category_id', 'target_id']);
    }

    public function product_type()
    {
        return $this->belongsTo(ProductType::class);
    }

    public function target()
    {
        return $this->belongsTo(Target::class)->withDefault();
    }

    public function orders()
    {
        return $this->morphToMany(Order::class, 'buyable');
    }

    public function registerMediaConversions(Media $media = null)
    {
        $this->addMediaConversion('thumb')
            ->width(366)
            ->keepOriginalImageFormat()
            ->height(366);
    }

    public function registerMediaCollections()
    {
        $this->addMediaCollection('product')->singleFile();

        $this->addMediaCollection('product_gallery');
    }

    public function getProductSizeAttribute()
    {
        if ($this->getAttribute('sizes')->count()) {
            $locale = config('app.locale');

            return Target::query()->where("name->$locale", $this->getAttribute('sizes')->keys()->first())->first()->getKey();
        }

        return null;
    }

    public function getImageAttribute()
    {
        if ($this->hasMedia('product')) {
            return asset($this->getFirstMediaUrl('product'));
        }

        return asset('img/no-image.jpg');
    }

    public function getThumbAttribute()
    {
        if ($this->hasMedia('product')) {
            return asset($this->getFirstMedia('product')->getUrl('thumb'));
        }

        return asset('img/no-image.jpg');
    }

    public function getImagesAttribute()
    {
        if ($this->hasMedia('product_gallery')) {
            return $this->getMedia('product_gallery')->map(function (Media $media) {
                return asset($media->getUrl());
            });
        }

        return collect();
    }
}
