<?php

namespace App\Models;

use App\Models\Pivot\MealPackTarget;
use App\Models\Pivot\ProductTarget;
use App\Models\Pivot\TargetCategory;
use App\Traits\HasTranslations;
use Illuminate\Database\Eloquent\Model;
use Spatie\Sluggable\HasSlug;
use Spatie\Sluggable\SlugOptions;
use Staudenmeir\EloquentHasManyDeep\HasRelationships;

class Target extends Model
{
    use HasRelationships, HasTranslations, HasSlug;

    public $timestamps = false;

    public $translatable = [
        'name',
    ];

    protected $fillable = [
        'id',
        'name',
        'slug',
    ];

    public function getSlugOptions(): SlugOptions
    {
        return SlugOptions::create()
            ->generateSlugsFrom('name')
            ->saveSlugsTo('slug');
    }

    public function meal_packs()
    {
        return $this->belongsToMany(MealPack::class, 'meal_pack_target')
            ->using(MealPackTarget::class);
    }

    public function posts()
    {
        return $this->hasMany(Post::class);
    }

    public function categories()
    {
        return $this->belongsToMany(Category::class, 'target_category')
            ->using(TargetCategory::class);
    }

    public function products()
    {
        return $this->belongsToMany(Product::class, 'product_target')
            ->using(ProductTarget::class);
    }

    public function product_sizes()
    {
        return $this->hasMany(ProductSize::class);
    }

    public function ingredients()
    {
        return $this->hasManyDeep(Ingredient::class, ['target_category', Category::class]);
    }
}
