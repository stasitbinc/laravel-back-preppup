<?php

namespace App\Models;

use App\Models\Pivot\TargetCategory;
use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    public $timestamps = false;

    protected $fillable = [
        'name',
    ];

    public function targets()
    {
        return $this->belongsToMany(Target::class, 'target_category')
            ->using(TargetCategory::class);
    }

    public function ingredients()
    {
        return $this->hasMany(Ingredient::class);
    }
}
