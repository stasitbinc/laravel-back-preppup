<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SubscriptionPlan extends Model
{
    protected $fillable = [
        'days',
        'one_meal',
        'two_meal',
        'three_meal',
        'hours_to_save',
    ];

    protected $guarded = [
        'id',
        'created_at',
    ];

    protected $dates = [
        'updated_at',
        'created_at',
    ];
}
