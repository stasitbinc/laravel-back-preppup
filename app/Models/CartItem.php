<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CartItem extends Model
{
    protected $fillable = [
        'cart_id',
        'buyable_type',
        'buyable_id',
        'target_id',
        'quantity',
        'quantity',
        'kcal',
    ];

    public function cart()
    {
        return $this->belongsTo(Cart::class);
    }

    public function buyable()
    {
        return $this->morphTo();
    }

    public function target()
    {
        return $this->belongsTo(Target::class);
    }
}
