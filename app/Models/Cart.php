<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Cart extends Model
{
    protected $withCount = ['cart_items'];

    protected $fillable = [
        'user_id',
    ];

    protected $appends = [
        'total',
        'total_products',
        'total_meal_packs',
    ];

    public function getTotalProductsAttribute()
    {
        /** @var CartItem $cartItem */
        $total = 0;

        $cartItems = $this->cart_items()->where('buyable_type', 'Product')->get();

        foreach ($cartItems as $cartItem) {
            $item = $cartItem->buyable()->first();

            $total += $item->getAttribute('price') * $cartItem->getAttribute('quantity');
        }

        return $total;
    }

    public function getTotalMealPacksAttribute()
    {
        /** @var CartItem $cartItem */
        $total = 0;

        $cartItems = $this->cart_items()->where('buyable_type', 'MealPack')->get();

        foreach ($cartItems as $cartItem) {
            $item = $cartItem->buyable()->first();

            $total += $item->getAttribute('price') * $cartItem->getAttribute('quantity');
        }

        return $total;
    }

    public function getTotalAttribute()
    {
        /** @var CartItem $cartItem */
        $total = 0;

        $cartItems = $this->cart_items()->get();

        foreach ($cartItems as $cartItem) {
            $item = $cartItem->buyable()->first();

            $total += $item->getAttribute('price') * $cartItem->getAttribute('quantity');
        }

        return $total;
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function cart_items()
    {
        return $this->hasMany(CartItem::class);
    }
}
