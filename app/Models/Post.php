<?php

namespace App\Models;

use App\Traits\HasTranslations;
use Illuminate\Database\Eloquent\Model;
use Spatie\MediaLibrary\HasMedia\HasMedia;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;
use Spatie\MediaLibrary\Models\Media;

class Post extends Model implements HasMedia
{
    use HasMediaTrait, HasTranslations;

    protected $fillable = [
        'title',
        'description',
        'name',
        'meal_pack_type_id',
    ];

    public $translatable = [
        'title',
        'description',
        'name',
    ];

    protected $guarded = [
        'id',
        'created_at',
    ];

    protected $dates = [
        'updated_at',
        'created_at',
    ];

    protected $hidden = [
        'media',
    ];

    protected $appends = [
        'image',
        'first_name',
    ];

    public function target()
    {
        return $this->belongsTo(Target::class);
    }

    public function registerMediaConversions(Media $media = null)
    {
        $this->addMediaConversion('thumb')
            ->width(366)
            ->height(366);
    }

    public function registerMediaCollections()
    {
        $this->addMediaCollection('post')->singleFile();
    }

    public function getFirstNameAttribute()
    {
        return strstr($this->getAttribute('name'), ' ', true);
    }

    public function getImageAttribute()
    {
        if ($this->hasMedia('post')) {
            return asset($this->getFirstMedia('post')->getUrl('thumb'));
        }

        return asset('img/no-image.jpg');
    }
}
