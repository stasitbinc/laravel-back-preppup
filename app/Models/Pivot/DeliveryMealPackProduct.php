<?php

namespace App\Models\Pivot;

use App\Models\Delivery;
use Illuminate\Database\Eloquent\Relations\Pivot;

class DeliveryMealPackProduct extends Pivot
{
    public $timestamps = false;

    protected $table = 'delivery_meal_pack_product';

    protected $fillable = [
        'delivery_id',
        'meal_pack_product_id',
    ];

    public function delivery()
    {
        return $this->belongsTo(Delivery::class);
    }

    public function meal_pack_product_id()
    {
        return $this->belongsTo(MealPackProduct::class);
    }
}
