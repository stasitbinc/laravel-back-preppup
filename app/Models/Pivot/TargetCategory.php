<?php

namespace App\Models\Pivot;

use App\Models\Category;
use App\Models\FaqCategory;
use App\Models\Product;
use App\Models\ProductSize;
use App\Models\Target;
use Illuminate\Database\Eloquent\Relations\Pivot;

class TargetCategory extends Pivot
{
    protected $table = 'target_category';

    protected $fillable = [
        'target_id',
        'category_id',
    ];

    public $timestamps = false;

    public function target()
    {
        return $this->belongsTo(Target::class);
    }

    public function category()
    {
        return $this->belongsTo(Category::class);
    }
}
