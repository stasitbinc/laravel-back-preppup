<?php

namespace App\Models\Pivot;

use App\Models\Product;
use App\Models\ProductSize;
use Illuminate\Database\Eloquent\Relations\Pivot;

class ProductProductSize extends Pivot
{
    protected $table = 'product_size';

    protected $fillable = [
        'product_id',
        'dish',
        'kcal',
        'sauce',
    ];

    protected $casts = [
        'dish' => 'object',
        'sauce' => 'object',
    ];

    protected $attributes = [
        'sauce' => '{}',
        'dish' => '{}',
    ];

    public $timestamps = false;

    public function product_size()
    {
        return $this->belongsTo(ProductSize::class);
    }

    public function product()
    {
        return $this->belongsTo(Product::class);
    }
}
