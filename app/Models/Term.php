<?php

namespace App\Models;

use App\Traits\HasTranslations;
use Illuminate\Database\Eloquent\Model;

class Term extends Model
{
    use HasTranslations;

    public $translatable = [
        'title',
        'answer',
    ];

    protected $fillable = [
        'title',
        'answer',
    ];

    protected $dates = [
        'updated_at',
        'created_at',
    ];
}
