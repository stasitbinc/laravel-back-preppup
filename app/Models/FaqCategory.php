<?php

namespace App\Models;

use App\Traits\HasTranslations;
use Illuminate\Database\Eloquent\Model;

class FaqCategory extends Model
{
    use HasTranslations;

    public $translatable = [
        'title',
    ];

    protected $fillable = [
        'title',
    ];

    protected $guarded = [
        'id',
        'created_at',
    ];

    protected $dates = [
        'updated_at',
        'created_at',
    ];

    public function faqs()
    {
        return $this->hasMany(Faq::class);
    }
}
