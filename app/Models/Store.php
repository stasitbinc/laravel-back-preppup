<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Store extends Model
{
    protected $fillable = [
        'name',
        'description',
    ];

    protected $dates = [
        'created_at',
        'updated_at',
    ];

    public function store_locations()
    {
        return $this->hasMany(StoreLocation::class);
    }
}
