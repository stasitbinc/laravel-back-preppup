<?php

namespace App\Models;

use App\Traits\HasTranslations;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Schema\Builder;
use Spatie\MediaLibrary\HasMedia\HasMedia;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;
use Spatie\MediaLibrary\Models\Media;
use Spatie\Sluggable\HasSlug;
use Spatie\Sluggable\SlugOptions;

class MealPackGroups extends Model implements HasMedia
{
    use HasSlug, HasTranslations, HasMediaTrait;

    public $translatable = [
        'title',
        'description',
        'sub_title',
    ];

    protected $fillable = [
        'id',
        'title',
        'sub_title',
        'description',
        'slug',
        'created_at',
    ];

    protected $hidden = [
        'created_at',
        'updated_at',
    ];

    protected $appends = [
        'image',
    ];

    public function getSlugOptions(): SlugOptions
    {
        return SlugOptions::create()
            ->generateSlugsFrom('title')
            ->saveSlugsTo('slug');
    }

    public function getRouteKeyName()
    {
        return 'slug';
    }

    public function meal_packs()
    {
        return $this->hasMany(MealPack::class);
    }

    public function registerMediaConversions(Media $media = null)
    {
        $this->addMediaConversion('thumb')
            ->width(366)
            ->keepOriginalImageFormat()
            ->height(366);
    }

    public function registerMediaCollections()
    {
        $this->addMediaCollection('pack')->singleFile();
    }

    public function getThumbAttribute()
    {
        if ($this->hasMedia('pack')) {
            return asset($this->getFirstMedia('pack')->getUrl('thumb'));
        }

        return asset('img/no-image.jpg');
    }

    public function getImageAttribute()
    {
        if ($this->hasMedia('pack')) {
            return asset($this->getFirstMediaUrl('pack'));
        }

        return asset('img/no-image.jpg');
    }

    public function scopeMealPacksId($query, $id)
    {
        return $query->whereHas('meal_packs', function ($query) use ($id) {
            $query->where('id', $id);
        });
    }

    public function scopeMealPacksKcal($query, $kcal)
    {
        return $query->whereHas('meal_packs', function ($query) use ($kcal) {
            $query->where('kcal', '=', $kcal);
        });
    }

    public function scopeMealCountPacksSlug($query, $slug)
    {
        return $query->whereHas('meal_packs.meal_pack_count_days', function ($query) use ($slug) {
            $query->where('slug', '=', $slug);
        });
    }

    public function scopeMealCountPacksId($query, $id)
    {
        return $query->whereHas('meal_packs.meal_pack_count_days', function ($query) use ($id) {
            $query->where('id', $id);
        });
    }

    public function scopeMealPackTargetSlug($query, $slug)
    {
        return $query->whereHas('meal_packs.target', function ($query) use ($slug) {
            $query->where('slug', $slug);
        });
    }
}
