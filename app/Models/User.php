<?php

namespace App\Models;

use App\Enums\GenderEnum;
use Illuminate\Contracts\Translation\HasLocalePreference;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use JoeDixon\Translation\Language;
use Konekt\Enum\Eloquent\CastsEnums;
use Laravel\Passport\HasApiTokens;
use Spatie\Permission\Traits\HasRoles;

class User extends Authenticatable implements HasLocalePreference
{
    use Notifiable, HasRoles, HasApiTokens, CastsEnums;

    protected $fillable = [
        'name',
        'age',
        'gender',
        'email',
        'phone',
        'password',
        'address',
        'floor',
        'postcode_id',
        'city',
        'language_id',
        'store_id',
        'is_repeat',
    ];

    protected $hidden = [
        'password',
        'remember_token',
    ];

    protected $casts = [
        'email_verified_at' => 'datetime',
        'is_repeat' => 'boolean',
    ];

    protected $appends = [
        'first_name',
        'last_name',
        'type',
    ];

    protected $attributes = [
        'gender' => GenderEnum::__default,
    ];

    protected $enums = [
        'gender' => GenderEnum::class,
    ];

    public function getNameAttribute($value)
    {
        return ucfirst($value);
    }

    public function getTypeAttribute()
    {
        $storeId = $this->getAttribute('store_id');

        if ($storeId) {
            return class_basename(StoreLocation::class);
        }

        return class_basename($this);
    }

    public function getFirstNameAttribute()
    {
        list($firstName) = explode(' ', $this->getAttribute('name'));

        return $firstName;
    }

    public function getLastNameAttribute()
    {
        return array_slice(explode(' ', $this->getAttribute('name')), -1)[0];
    }

    public function findForPassport($email)
    {
        return $this->where('email', $email)->first();
    }

    public function preferredLocale()
    {
        return config('app.locale');
    }

    public function cart()
    {
        return $this->hasOne(Cart::class);
    }

    public function cart_items()
    {
        return $this->hasManyThrough(CartItem::class, Cart::class);
    }

    public function orders()
    {
        return $this->hasMany(Order::class, 'user_id', 'id');
    }

    public function postcode()
    {
        return $this->belongsTo(Postcode::class);
    }

    public function language()
    {
        return $this->belongsTo(Language::class);
    }

    public function subscriptions()
    {
        return $this->hasMany(Subscription::class);
    }
}
