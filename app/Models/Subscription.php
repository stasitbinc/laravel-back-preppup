<?php

namespace App\Models;

use App\Enums\SubscriptionStatusEnum;
use Illuminate\Database\Eloquent\Model;
use Konekt\Enum\Eloquent\CastsEnums;

class Subscription extends Model
{
    use CastsEnums;

    protected $fillable = [
        'user_id',
        'meal_pack_id',
        'order_id',
        'status',
        'attempts_count',
    ];

    protected $dates = [
        'updated_at',
        'created_at',
    ];

    protected $attributes = [
        'status' => SubscriptionStatusEnum::__default,
    ];

    protected $enums = [
        'status' => SubscriptionStatusEnum::class,
    ];

    public function buyable()
    {
        return $this->belongsTo(Buyable::class);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function order()
    {
        return $this->belongsTo(Order::class);
    }
}
