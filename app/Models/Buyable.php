<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Buyable extends Model
{
    public $timestamps = false;

    protected $dateFormat = 'Y-m-d';

    protected $fillable = [
        'order_id',
        'buyable_type',
        'buyable_id',
        'target_id',
        'quantity',
        'date',
        'kcal',
        'price',
    ];

    protected $dates = [
        'date',
    ];

    public function subscriptions()
    {
        return $this->hasMany(Subscription::class);
    }

    public function order()
    {
        return $this->belongsTo(Order::class);
    }

    public function target()
    {
        return $this->belongsTo(Target::class);
    }

    public function buyable()
    {
        return $this->morphTo()->withTrashed();
    }
}
