<?php

namespace App\Models;

use App\Models\Pivot\ProductTarget;
use Illuminate\Database\Eloquent\Model;

class Ingredient extends Model
{
    public $timestamps = false;

    protected $fillable = [
        'name',
        'calories',
        'fat',
        'carbohydrates',
        'protein',
        'category_id',
    ];

    protected $casts = [
        'calories' => 'float',
        'fat' => 'float',
        'carbohydrates' => 'float',
        'protein' => 'float',
    ];

    public function category()
    {
        return $this->belongsTo(Category::class);
    }

    public function products()
    {
        return $this->belongsToMany(Product::class, 'product_target')
            ->using(ProductTarget::class)
            ->withPivot(['size']);
    }

    public function targets()
    {
        return $this->belongsToMany(Target::class, 'product_target')
            ->using(ProductTarget::class)
            ->withPivot(['size', 'product_id', 'category_id']);
    }
}
