<?php

namespace App\Models;

use App\Traits\HasTranslations;
use Illuminate\Database\Eloquent\Model;
use Spatie\Sluggable\HasSlug;
use Spatie\Sluggable\SlugOptions;

class TypeFood extends Model
{
    use HasTranslations, HasSlug;

    public $translatable = [
        'title',
        'mini_description',
        'description',
        'slug',
    ];

    protected $fillable = [
        'title',
        'mini_description',
        'description',
        'answer',
    ];

    protected $dates = [
        'updated_at',
        'created_at',
    ];

    public function getSlugOptions(): SlugOptions
    {
        return SlugOptions::create()
            ->generateSlugsFrom('title')
            ->saveSlugsTo('slug');
    }
}
