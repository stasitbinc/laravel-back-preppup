<?php

namespace App\Models;

use App\Enums\OrderStatusEnum;
use Illuminate\Database\Eloquent\Model;
use Konekt\Enum\Eloquent\CastsEnums;

class Order extends Model
{
    use CastsEnums;

    protected $withCount = ['buyables', 'meal_packs', 'products'];

    protected $fillable = [
        'user_id',
        'coupon_id',
        'overview',
        'allergy',
        'delivery',
        'address',
        'payment',
        'status',
        'amount',
        'date',
        'step',
        'note',
    ];

    protected $attributes = [
        'status' => OrderStatusEnum::__default,
        'overview' => '{}',
        'allergy' => '{}',
        'delivery' => '{}',
        'address' => '{}',
        'payment' => '{}',
        'amount' => '{}',
    ];

    protected $casts = [
        'is_same' => 'boolean',
        'overview' => 'object',
        'allergy' => 'object',
        'delivery' => 'object',
        'address' => 'object',
        'payment' => 'object',
        'amount' => 'object',
    ];

    protected $dates = [
        'updated_at',
        'created_at',
        'date',
    ];

    protected $enums = [
        'status' => OrderStatusEnum::class,
    ];

    protected $appends = [
        'uuid',
        'checkvaluemd5',
        'description',
        'download',
        'address_value',
        'city',
        'phone',
        'floor',
        'postcode_id',
        'is_same',
        'is_free',
        'signature',
    ];

    public function getSignatureAttribute()
    {
        $data = [
            config('services.netgiro.secret_key'),
            $this->getKey(),
            (string) $this->getAttribute('amount')->for_payment,
            config('services.netgiro.app_id'),
        ];

        return hash('sha256', implode('', $data));
    }

    public function getIsFreeAttribute()
    {
        $coupon = $this->coupon()->first();

        if ($coupon && $coupon->getAttribute('products') == 100) {
            return true;
        }

        return false;
    }

    public function getIsSameAttribute()
    {
        if (property_exists($this->getAttribute('address'), 'is_same')) {
            return $this->getAttribute('address')->is_same ?? null;
        }

        return true;
    }

    public function getAddressValueAttribute()
    {
        if (property_exists($this->getAttribute('address'), 'is_same')) {
            $isSame = $this->getAttribute('address')->is_same;

            if ($isSame) {
                return $this->user()->first()->getAttribute('address') ?? null;
            }

            return $this->getAttribute('address')->address ?? null;
        }

        return $this->user()->first()->getAttribute('address') ?? null;
    }

    public function getPhoneAttribute()
    {
        if (property_exists($this->getAttribute('address'), 'is_same')) {
            $isSame = $this->getAttribute('address')->is_same;

            if ($isSame) {
                return $this->user()->first()->getAttribute('phone') ?? null;
            }

            return $this->getAttribute('address')->phone ?? null;
        }

        return $this->user()->first()->getAttribute('phone') ?? null;
    }

    public function getCityAttribute()
    {
        if (property_exists($this->getAttribute('address'), 'is_same')) {
            $isSame = $this->getAttribute('address')->is_same;

            if ($isSame) {
                return $this->user()->first()->getAttribute('city') ?? null;
            }

            return $this->getAttribute('address')->city ?? null;
        }

        return $this->user()->first()->getAttribute('city') ?? null;
    }

    public function getFloorAttribute()
    {
        if (property_exists($this->getAttribute('address'), 'is_same')) {
            $isSame = $this->getAttribute('address')->is_same;

            if ($isSame) {
                return $this->user()->first()->getAttribute('floor') ?? null;
            }

            return $this->getAttribute('address')->floor ?? null;
        }

        return $this->user()->first()->getAttribute('floor') ?? null;
    }

    public function getPostcodeIdAttribute()
    {
        if (property_exists($this->getAttribute('address'), 'is_same')) {
            $isSame = $this->getAttribute('address')->is_same;

            if ($isSame) {
                return $this->user()->first()->getAttribute('postcode_id') ?? null;
            }

            return $this->getAttribute('address')->postcode_id ?? null;
        }

        return $this->user()->first()->getAttribute('postcode_id') ?? null;
    }

    public function getDownloadAttribute()
    {
        return route('download', ['order' => $this->getKey()]);
    }

    public function getCheckvaluemd5Attribute()
    {
        $isTest = config('services.korta.env') ? 'TEST' : '';

        $data = (string) $this->getAttribute('amount')->for_payment
            .'ISK'
            .config('services.korta.merchant')
            .config('services.korta.terminal')
            .htmlentities($this->getAttribute('description'))
            .'/'
            .'STORAGE'
            .'/'
            .'0'
            .'/'
            .optional(optional($this->getAttribute('payment'))->korta)->token
            .'/'
            .'M';

        return md5($data.config('services.korta.secret').$isTest);
    }

    public function getDescriptionAttribute()
    {
        $description = "ORDER: #{$this->getAttribute('uuid')}<br><br>PRODUCTS:<br>";

        $buyables = $this->buyables()->get();

        foreach ($buyables as $buyable) {
            $description .= "{$buyable->getAttribute('quantity')} x {$buyable->getAttribute('price')} kr. - {$buyable->buyable()->first()->getAttribute('name')}<br><br>";
        }

        $description .= "TOTAL: {$this->getAttribute('amount')->for_payment} kr.<br>";

        return $description;
    }

    public function getUuidAttribute()
    {
        return 'P0054'.str_pad($this->getKey(), 7, 0, STR_PAD_LEFT);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function coupon()
    {
        return $this->belongsTo(Coupon::class);
    }

    public function products()
    {
        return $this->morphedByMany(Product::class, 'buyable')
            ->withPivot(['quantity', 'target_id', 'date', 'price']);
    }

    public function meal_packs()
    {
        return $this->morphedByMany(MealPack::class, 'buyable')
            ->withPivot(['quantity', 'target_id', 'date', 'price']);
    }

    public function buyables()
    {
        return $this->hasMany(Buyable::class);
    }

    public function subscriptions()
    {
        return $this->hasMany(Subscription::class);
    }

    public function postcode()
    {
        return $this->belongsTo(Postcode::class);
    }

    public function deliveries()
    {
        return $this->hasMany(Delivery::class);
    }
}
