<?php

namespace App\Http\Controllers\Api;

use App\Enums\SubscriptionStatusEnum;
use App\Http\Controllers\Controller;
use App\Http\Resources\Subscription\SubscriptionCollection;
use App\Http\Resources\Subscription\SubscriptionResource;
use App\Models\Subscription;
use App\Models\User;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class SubscriptionController extends Controller
{
    public function index(Request $request)
    {
        /** @var User $user */
        $user = $request->user('api');

        $subscriptions = $user->subscriptions()
            ->where('status', SubscriptionStatusEnum::ACTIVE)
            ->whereHas('buyable', function (Builder $query) {
                $query->where('buyable_type', 'MealPack');
            })
            ->with('buyable.buyable.products')
            ->get();

        return new SubscriptionCollection($subscriptions);
    }

    public function cancel(Subscription $subscription, Request $request)
    {
        abort_if(now()->lt($subscription->getAttribute('created_at')->addWeeks(2)),
            Response::HTTP_FORBIDDEN,
            __("You can't cancel subscription for now."));

        $subscription->update([
            'status' => SubscriptionStatusEnum::CANCELED,
        ]);

        return (new SubscriptionResource($subscription))
            ->additional([
                'message' => __('You successfully canceled subscription.'),
            ])
            ->toResponse($request);
    }
}
