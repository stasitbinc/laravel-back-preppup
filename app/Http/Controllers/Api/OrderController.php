<?php

namespace App\Http\Controllers\Api;

use App\Enums\OrderStatusEnum;
use App\Http\Controllers\Controller;
use App\Http\Requests\CouponRequest;
use App\Http\Requests\NetgiroRequest;
use App\Http\Requests\OrderRequest;
use App\Http\Requests\PaymentRequest;
use App\Http\Resources\Coupon\CouponResource;
use App\Http\Resources\Order\OrderCollection;
use App\Http\Resources\Order\OrderResource;
use App\Models\Cart;
use App\Models\Coupon;
use App\Models\Order;
use App\Models\User;
use App\Payment\Korta\Client as KortaClient;
use App\Payment\Netgiro\Client as NetgiroClient;
use App\Payment\Pei\Client as PeiClient;
use App\Services\OrderService;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Symfony\Component\HttpFoundation\Response;

class OrderController extends Controller
{
    private $client;
    private $service;
    private $peiClient;
    private $netgiroClient;

    public function __construct(KortaClient $client, PeiClient $peiClient, NetgiroClient $netgiroClient, OrderService $service)
    {
        $this->client = $client;
        $this->service = $service;
        $this->peiClient = $peiClient;
        $this->netgiroClient = $netgiroClient;
    }

    public function index(Request $request)
    {
        /** @var User $user */
        $user = $request->user('api');

        $orders = $user->orders()
            ->with('buyables.buyable')
            ->whereNotNull('date')
            ->whereIn('status', [OrderStatusEnum::COMPLETED, OrderStatusEnum::PAID])
            ->orderByDesc('date')
            ->get();

        return new OrderCollection($orders);
    }

    public function exportPdf(Order $order)
    {
        $pdf = \PDF::loadView('export', compact('order'))->output();

        return new Response($pdf, 200, [
            'Content-Type' => 'application/pdf',
        ]);
    }

    public function show(Order $order)
    {
        return new OrderResource($order->load(['buyables.buyable', 'coupon']));
    }

    public function updateStep(Order $order, Request $request)
    {
        $currentStep = $request->get('current_step');

        $order->forceFill([
            "{$currentStep}->done" => true,
        ])->save();

        return new OrderResource($order->load(['buyables.buyable', 'coupon']));
    }

    public function checkAndPay(Order $order, PaymentRequest $request)
    {
        /** @var User $user */
        $user = $request->user('api');

        $card = Arr::get($request->validated(), 'card4');

        $response = [
            'd2brand' => Arr::get($request->validated(), 'cardbrand'),
            'd2dsp' => "**** **** **** $card",
        ];

        if ($order->getAttribute('checkvaluemd5') == $request->get('checkvaluemd5')) {
            $this->service->success($order, $request->validated(), $user, $response, true);
        } else {
            $this->service->error($order, $user, $response, true);
        }

//        $response = $this->client->voidWebPayment($order, $request);

//        $responseCode = Arr::get($response, 'd39');

        /*if ($responseCode == 000 && $order->getAttribute('checkvaluemd5') == $request->get('checkvaluemd5')) {

            $order->forceFill([
                'payment->korta->transaction' => Arr::get($response, 'd56'),
            ])->save();

            $cofResponse = $this->client->cofPayment($order, $order->getAttribute('amount')->total);

            $cofResponseCode = Arr::get($cofResponse, 'd39');

            if ($cofResponseCode == 000) {
                $this->service->success($order, $request->validated(), $user, $cofResponse, true);
            }

            if ($cofResponseCode != 000) {
                $this->service->error($order, $user, $cofResponse, true);
            }
        }*/

        /*if ($responseCode != 000) {
            $this->service->error($order, $user, $response, true);
        }*/

        return new OrderResource($order->load(['buyables.buyable', 'coupon']));
    }

    public function checkStep(OrderRequest $request, Order $order)
    {
        /** @var User $user */

        /** @var Cart $cart */

        /** @var Order $order */
        $user = $request->user('api');

        $cart = $user->cart()->first();

        $amount = $this->service->calculateOrderAmount($user, $request);

        $request->merge(compact('amount'));

        if (! $order->getKey() && $user->orders()->latest()->where('status', OrderStatusEnum::PENDING)->exists()) {
            $order = $user->orders()->latest()->where('status', OrderStatusEnum::PENDING)->first();
        }

        $order = $user->orders()->updateOrCreate(['id' => $order->getKey()], $request->all());

        $this->service->connectItemsWithOrder($order, $cart, $request);

        return new OrderResource($order->load(['buyables.buyable', 'coupon']));
    }

    public function checkCoupon(CouponRequest $request)
    {
        /** @var Order $order */
        $order = Order::query()->find($request->get('order_id'));

        $coupon = Coupon::query()
            ->where('code', $request->get('code'))
            ->first();

        $order->coupon()->associate($coupon);

        $order->save();

        return (new CouponResource($coupon))
            ->additional([
                'message' => __('You successfully used coupon code :code', ['code' => $coupon->getAttribute('code')]),
            ])
            ->toResponse($request);
    }

    public function destroy(Order $order)
    {
        if ($order->getAttribute('status')->isPending()) {
            $order->delete();
        }

        return new OrderResource($order);
    }

    public function complete(Order $order)
    {
        /** @var Coupon $coupon */
        $coupon = $order->coupon()->first();

        abort_if(! $this->service->checkCouponFree($coupon), Response::HTTP_FORBIDDEN, __('Payment error'));

        $this->service->confirmFreeOrder($order);

        return new OrderResource($order);
    }

    public function getPaiPaymentUrl(Order $order)
    {
        return response()->json(['url' => $this->peiClient->pay($order)]);
    }

    public function checkAndPayPei(Order $order)
    {
        /** @var User $user */
        $user = $order->user()->first();

        $response = $this->peiClient->getOrder($order);

        abort_if(Arr::get($response, 'orderId') != $order->getAttribute('payment')->pei->id, Response::HTTP_FORBIDDEN, __('Payment error'));

        $status = Arr::get($response, 'isConfirmed');

        if ($status) {
            $this->service->confirmFreeOrder($order);
        } else {
            $this->service->error($order, $user, $response, true);
        }

        return new OrderResource($order);
    }

    public function netgiroConfirm(NetgiroRequest $request)
    {
        /** @var Order $order */

        /** @var User $user */
        $orderId = $request->get('referenceNumber');

        $order = Order::query()->findOrFail($orderId);

        $user = $order->user()->first();

        $status = filter_var($request->get('success'), FILTER_VALIDATE_BOOLEAN);

        if ($status) {
            $this->service->confirmFreeOrder($order);
        } else {
            $this->service->error($order, $user, [], true);
        }

        return new OrderResource($order);
    }
}
