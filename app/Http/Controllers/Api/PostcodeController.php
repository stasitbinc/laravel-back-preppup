<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Resources\Postcode\PostcodeCollection;
use App\Http\Resources\Postcode\PostcodeResource;
use App\Models\Postcode;

class PostcodeController extends Controller
{
    public function index()
    {
        $postcodes = Postcode::all();

        return new PostcodeCollection($postcodes);
    }

    public function show(Postcode $postcode)
    {
        return new PostcodeResource($postcode);
    }
}
