<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Resources\FaqCategory\FaqCategoryCollection;
use App\Models\FaqCategory;

class FaqCategoryController extends Controller
{
    public function index()
    {
        $faqCategories = FaqCategory::query()->has('faqs')->with('faqs')->get();

        return new FaqCategoryCollection($faqCategories);
    }
}
