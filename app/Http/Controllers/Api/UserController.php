<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\UserForgotPasswordRequest;
use App\Http\Requests\UserRegisterRequest;
use App\Http\Requests\UserSubscribeRequest;
use App\Http\Requests\UserUpdateRequest;
use App\Http\Resources\User\UserResource;
use App\Models\User;
use App\Notifications\ForgotPasswordNotification;
use Illuminate\Auth\Events\Registered;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Str;
use JoeDixon\Translation\Language;
use Spatie\Newsletter\NewsletterFacade as Newsletter;

class UserController extends Controller
{
    public function subscribe(UserSubscribeRequest $request)
    {
        /** @var User $user */
        abort_if(Newsletter::isSubscribed($request->get('subscription_email')),
            Response::HTTP_FORBIDDEN,
            __("You've already subscribed for Preppup mailing list."));

        Newsletter::subscribe($request->get('subscription_email'));

        return response()->json(['message' => __('You have been successfully subscribed for Preppup mailing list.')]);
    }

    public function forgot(UserForgotPasswordRequest $request)
    {
        /** @var User $user */
        $user = User::query()->where('email', $request->get('forgot_email'))->firstOrFail();

        $password = Str::random(10);

        $user->update([
            'password' => bcrypt($password),
        ]);

        $user->notify((new ForgotPasswordNotification($password))->locale(config('app.locale')));

        return (new UserResource($user))
            ->additional([
                'message' => __('You have been successfully restored password. Please go to you email and check message.'),
            ])
            ->toResponse($request);
    }

    public function show(Request $request)
    {
        /** @var User $user */
        $user = $request->user('api');

        return new UserResource($user->load(['cart', 'postcode', 'language']));
    }

    public function logout(Request $request)
    {
        /** @var User $user */
        $user = $request->user('api');

        $user->token()->revoke();

        return new UserResource($user);
    }

    public function store(UserRegisterRequest $request)
    {
        /** @var User $user */
        $password = bcrypt($request->get('new_password'));

        $user = User::query()->create($request->validated() + compact('password'));

        event(new Registered($user));

        return (new UserResource($user))
            ->additional([
                'message' => __('You have been successfully registered. Please login with your credentials.'),
            ])
            ->toResponse($request);
    }

    public function language(Request $request)
    {
        /** @var User $user */
        $user = $request->user('api');

        $lng = $request->get('language') ?? 'is';

        $language = Language::query()->where('language', $lng)->first();

        $user->update([
            'language_id' => $language->getKey(),
        ]);

        return new UserResource($user);
    }

    public function update(UserUpdateRequest $request)
    {
        /** @var User $user */
        $user = $request->user('api');

        $user->update($request->validated());

        if ($request->has('new_password')) {
            $password = bcrypt($request->get('new_password'));

            $user->update(compact('password'));
        }

        return (new UserResource($user))
            ->additional([
                'message' => __('Your settings have been saved.'),
            ])
            ->toResponse($request);
    }
}
