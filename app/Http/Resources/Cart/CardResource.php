<?php

namespace App\Http\Resources\Cart;

use App\Http\Resources\CartItem\CardItemCollection;
use App\Http\Resources\Order\OrderResource;
use App\Http\Resources\User\UserResource;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class CardResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param Request $request
     * @return array
     */
    public function toArray($request)
    {
        return array_merge(parent::toArray($request), [
            'user' => new UserResource($this->whenLoaded('user')),
            'cart_items' => new CardItemCollection($this->whenLoaded('cart_items')),
            'order' => new OrderResource($this->whenLoaded('order')),
        ]);
    }
}
