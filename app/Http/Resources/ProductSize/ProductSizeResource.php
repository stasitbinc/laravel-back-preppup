<?php

namespace App\Http\Resources\ProductSize;

use App\Http\Resources\Product\ProductCollection;
use App\Http\Resources\Target\TargetResource;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class ProductSizeResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param Request $request
     * @return array
     */
    public function toArray($request)
    {
        return
            [
                'target' => new TargetResource($this->target),
                'dish' => json_decode($this->dish),
                'sauce' => json_decode($this->sauce),
                'kcal' => $this->kcal,
            ];
    }
}
