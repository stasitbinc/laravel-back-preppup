<?php

namespace App\Http\Resources\FaqCategory;

use App\Http\Resources\Faq\FaqCollection;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class FaqCategoryResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return array_merge(parent::toArray($request), [
            'faqs' => new FaqCollection($this->whenLoaded('faqs')),
        ]);
    }
}
