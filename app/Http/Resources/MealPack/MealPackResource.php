<?php

namespace App\Http\Resources\MealPack;

use App\Http\Resources\MealPackCountDay\MealPackCountDayResource;
use App\Http\Resources\MealPackGroup\MealPackGroupResource;
use App\Http\Resources\MealPackInfo\MealPackInfoResource;
use App\Http\Resources\MealPackTarget\MealPackTargetResource;
use App\Http\Resources\Product\ProductResource;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class MealPackResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param Request $request
     * @return array
     */
    public function toArray($request) :array
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'slug' => $this->slug,
            'sub_title' => $this->sub_title,
            'allergy' => $this->allergy,
            'description' => $this->description,
            'short_description' => $this->short_description,
            'price' => $this->price,
            'price_meal' => $this->price_meal,
            'kcal' => $this->kcal,
            'per_type' => $this->per_type,
            'is_public' => $this->is_public,
            'created_at' => $this->created_at,
            'images' => $this->getImageAttribute(),
            'weeks' => $this->weeks,
            'meal_pack_infos' => MealPackInfoResource::collection($this->whenLoaded('meal_pack_infos')),
            'meal_pack_groups' => new MealPackGroupResource($this->meal_pack_groups),
            'meal_pack_count_day' => new MealPackCountDayResource($this->meal_pack_count_days),
            'targets_id' => $this->resource->targets->pluck('id') ?? [],
            'targets' => MealPackTargetResource::collection($this->whenLoaded('targets')),
            'products' => ProductResource::collection($this->whenLoaded('products')),
        ];
    }
}
