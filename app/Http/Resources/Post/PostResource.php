<?php

namespace App\Http\Resources\Post;

use App\Http\Resources\MealPackType\MealPackTypeResource;
use App\Http\Resources\Target\TargetResource;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class PostResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param Request $request
     * @return array
     */
    public function toArray($request)
    {
        return array_merge(parent::toArray($request), [
            'target' => new TargetResource($this->whenLoaded('target')),
        ]);
    }
}
