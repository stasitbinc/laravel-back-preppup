<?php

namespace App\Http\Resources\User;

use App\Http\Resources\Cart\CardResource;
use App\Http\Resources\Language\LanguageResource;
use App\Http\Resources\Order\OrderCollection;
use App\Http\Resources\Postcode\PostcodeResource;
use App\Http\Resources\Subscription\SubscriptionCollection;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class UserResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param Request $request
     * @return array
     */
    public function toArray($request)
    {
        return array_merge(parent::toArray($request), [
            'cart' => new CardResource($this->whenLoaded('cart')),
            'orders' => new OrderCollection($this->whenLoaded('orders')),
            'postcode' => new PostcodeResource($this->whenLoaded('postcode')),
            'language' => new LanguageResource($this->whenLoaded('language')),
            'subscriptions' => new SubscriptionCollection($this->whenLoaded('subscriptions')),
            'gender_label' => $this->gender->label(),
        ]);
    }
}
