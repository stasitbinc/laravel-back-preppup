<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UserSubscribeRequest extends FormRequest
{
    public function rules()
    {
        return [
            'subscription_email' => [
                'required',
                'string',
                'email',
            ],
        ];
    }

    public function attributes()
    {
        return [
            'subscription_email' => __('email'),
        ];
    }
}
