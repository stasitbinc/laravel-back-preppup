<?php

namespace App\Http\Requests;

use App\Enums\GenderEnum;
use App\Models\Postcode;
use App\Models\User;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class UserUpdateRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => [
                'required',
                'string',
            ],
            'age' => [
                'required',
                'numeric',
            ],
            'gender' => [
                'required',
                'string',
                Rule::in(GenderEnum::toArray()),
            ],
            'address' => [
                'required',
                'string',
            ],
            'postcode_id' => [
                'required',
                'integer',
                Rule::exists(app(Postcode::class)->getTable(), 'id'),
            ],
            'email' => [
                'required',
                'string',
                Rule::unique(app(User::class)->getTable(), 'email')->ignore($this->get('id')),
            ],
            'floor' => [
                'required',
                'string',
            ],
            'phone' => [
                'required',
                'string',
            ],
            'city' => [
                'required',
                'string',
            ],
            'new_password' => [
                'sometimes',
                'confirmed',
            ],
            'new_password_confirmation' => [
                'sometimes',
            ],
        ];
    }

    public function attributes()
    {
        return [
            'name' => __('Name'),
            'email' => __('email'),
            'city' => __('city'),
            'phone' => __('phone'),
            'address' => __('address'),
            'postcode_id' => __('postcode'),
            'floor' => __('floor'),
            'new_password' => __('Password'),
            'new_password_confirmation' => __('Confirm Password'),
        ];
    }
}
