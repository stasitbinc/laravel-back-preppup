<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class PaymentRequest extends FormRequest
{
    public function rules()
    {
        return [
            'checkvaluemd5' => [
                'required',
                'string',
            ],
            'time' => [
                'required',
                'date',
                'date_format:d-m-Y H:i:s',
            ],
            'cardbrand' => [
                'required',
                'string',
            ],
            'card4' => [
                'required',
                'string',
            ],
            'downloadmd5' => [
                'required',
                'string',
            ],
            'authcode' => [
                'required',
                'string',
            ],
        ];
    }
}
