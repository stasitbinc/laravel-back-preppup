<?php

namespace App\Http\Requests;

use App\Models\User;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class UserForgotPasswordRequest extends FormRequest
{
    public function rules()
    {
        return [
            'forgot_email' => [
                'required',
                'string',
                'email',
                Rule::exists(app(User::class)->getTable(), 'email'),
            ],
        ];
    }

    public function attributes()
    {
        return [
            'forgot_email' => __('email'),
        ];
    }
}
