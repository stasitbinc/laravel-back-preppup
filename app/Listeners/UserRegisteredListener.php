<?php

namespace App\Listeners;

use App\Models\User;
use App\Services\ActiveCampaignService;
use Exception;
use Illuminate\Auth\Events\Registered;
use Illuminate\Contracts\Queue\ShouldQueue;

class UserRegisteredListener implements ShouldQueue
{
    protected $service;

    /**
     * Create the event listener.
     *
     * @param ActiveCampaignService $service
     */
    public function __construct(ActiveCampaignService $service)
    {
        $this->service = $service;
    }

    /**
     * Handle the event.
     *
     * @param Registered $event
     * @return void
     * @throws Exception
     */
    public function handle(Registered $event): void
    {
        /** @var User $user */
        $user = $event->user;

        $this->service->sendContact($user);
    }
}
