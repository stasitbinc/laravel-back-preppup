<?php

namespace App\Providers;

use App\Models\Buyable;
use App\Models\Coupon;
use App\Models\MealPack;
use App\Models\Order;
use App\Models\Product;
use App\Observers\BuyableObserver;
use App\Observers\CouponObserver;
use App\Observers\OrderObserver;
use App\Observers\ProductObserver;
use App\Payment\Korta\Client as KortaClient;
use App\Payment\Korta\Config as KortaConfig;
use App\Payment\Netgiro\Client as NetgiroClient;
use App\Payment\Netgiro\Config as NetgiroConfig;
use App\Payment\Pei\Client as PeiClient;
use App\Payment\Pei\Config as PeiConfig;
use App\Sending\Client as SendingClient;
use App\Sending\Config as SendingConfig;
use Illuminate\Database\Eloquent\Relations\Relation;
use Illuminate\Support\Facades\App;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        if ($this->app->isLocal()) {
            $this->app->register(TelescopeServiceProvider::class);
        }

        $this->app->singleton(KortaClient::class, function () {
            $config = new KortaConfig(
                config('services.korta.api_endpoint'),
                config('services.korta.user'),
                config('services.korta.pwd'),
                config('services.korta.api_terminal'),
                config('services.korta.api_merchant')
            );

            return KortaClient::create($config);
        });

        $this->app->singleton(PeiClient::class, function () {
            $env = App::environment();

            $config = new PeiConfig(
                config("services.pei.$env.auth_url"),
                config("services.pei.$env.order_url"),
                config("services.pei.$env.payment_url"),
                config("services.pei.$env.client_id"),
                config("services.pei.$env.secret_key"),
                config("services.pei.$env.merchant_id")
            );

            return PeiClient::create($config);
        });

        $this->app->singleton(NetgiroClient::class, function () {
            $config = new NetgiroConfig(
                config('services.netgiro.url'),
                config('services.netgiro.app_id'),
                config('services.netgiro.secret_key')
            );

            return NetgiroClient::create($config);
        });

        $this->app->singleton(SendingClient::class, function () {
            $config = new SendingConfig(
                config('services.sending.api_url'),
                config('services.sending.api_key')
            );

            return SendingClient::create($config);
        });
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Order::observe(OrderObserver::class);
        Coupon::observe(CouponObserver::class);
        Product::observe(ProductObserver::class);
        Buyable::observe(BuyableObserver::class);

        Relation::morphMap([
            'Product' => Product::class,
            'MealPack' => MealPack::class,
        ]);
    }
}
