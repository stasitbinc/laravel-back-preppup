<?php

namespace App\Services;

use App\Models\User;
use Gentor\ActiveCampaign\Facades\ActiveCampaign as AC;
use Illuminate\Support\Facades\Log;

class ActiveCampaignService
{
    public function sendContact(User $user): void
    {
        $contact = [
            'email' => $user->getAttribute('email'),
            'first_name' => $user->getAttribute('first_name'),
            'last_name' => $user->getAttribute('last_name'),
            'phone' => $user->getAttribute('phone'),
        ];

        try {
            AC::contactSync($contact);
        } catch (\Exception $exception) {
            Log::critical($exception->getMessage());
            throw $exception;
        }
    }
}
