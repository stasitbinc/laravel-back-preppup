<?php

namespace App\Enums;

use Konekt\Enum\Enum;

class ReservationOrderEnum extends Enum
{
    const __default = self::SUBSCRIBE;

    const SUBSCRIBE = 'subscribe';
    const ONCE = 'once';

    public static $labels = [];

    protected static function boot()
    {
        static::$labels = [
            self::SUBSCRIBE => __('I want to subscribe'),
            self::ONCE => __('I want to buy once'),
        ];
    }
}
