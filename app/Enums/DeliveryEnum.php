<?php

namespace App\Enums;

use Konekt\Enum\Enum;

class DeliveryEnum extends Enum
{
    const __default = self::DAY;

    const DAY = 'day';
    const DAYS = 'days';

    public static $labels = [];

    protected static function boot()
    {
        static::$labels = [
            self::DAY => __('I want one delivery day'),
            self::DAYS => __('I want to distribute delivery days'),
        ];
    }
}
