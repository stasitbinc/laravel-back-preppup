<?php

namespace App\Nova\Metrics;

use App\Enums\OrderStatusEnum;
use App\Models\Order;
use Laravel\Nova\Metrics\Partition;

class OrdersPerStatus extends Partition
{
    /**
     * Calculate the value of the metric.
     *
     * @return mixed
     */
    public function calculate()
    {
        $orders = Order::all()->groupBy(function (Order $order) {
            return $order->getAttribute('status')->label();
        })->map(function ($item) {
            return collect($item)->count();
        });

        return $this->result($orders->toArray())->colors([
            OrderStatusEnum::COMPLETED()->label() => '#68c159',
            OrderStatusEnum::PAID()->label() => '#3c5565',
            OrderStatusEnum::PENDING()->label() => '#9daab2',
            OrderStatusEnum::FAILED()->label() => '#e76354',
        ]);
    }

    /**
     * Determine for how many minutes the metric should be cached.
     *
     * @return  \DateTimeInterface|\DateInterval|float|int
     */
    public function cacheFor()
    {
        return now()->addMinutes(5);
    }

    /**
     * Get the URI key for the metric.
     *
     * @return string
     */
    public function uriKey()
    {
        return 'orders-per-status';
    }
}
