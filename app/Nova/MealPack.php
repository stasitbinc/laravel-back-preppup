<?php

namespace App\Nova;

use Ebess\AdvancedNovaMediaLibrary\Fields\Images;
use Illuminate\Http\Request;
use Laravel\Nova\Fields\BelongsTo;
use Laravel\Nova\Fields\BelongsToMany;
use Laravel\Nova\Fields\Boolean;
use Laravel\Nova\Fields\ID;
use Laravel\Nova\Fields\Select;
use Laravel\Nova\Fields\Textarea;
use Preppup\MealPackInfo\MealPackInfo;
use Preppup\MealPackSizesViewer\MealPackSizesViewer;
use R64\NovaFields\Currency;
use R64\NovaFields\JSON;
use R64\NovaFields\Number;
use Spatie\NovaTranslatable\Translatable;
use Yassi\NestedForm\NestedForm;

class MealPack extends Resource
{
    /**
     * The model the resource corresponds to.
     *
     * @var string
     */
    public static $model = \App\Models\MealPack::class;

    public function title()
    {
        return 'name';
    }

    public static $group = '<span class="hidden">30</span>Meal Packs';

    /**
     * The columns that should be searched.
     *
     * @var array
     */
    public static $search = [
        'id',
        'name',
        'sub_title',
        'description',
        'price',
        'price_meal',
        'kcal',
        'weeks',
    ];

    /**
     * Get the fields displayed by the resource.
     *
     * @param Request $request
     * @return array
     */
    public function fields(Request $request)
    {
        return [
            ID::make()->sortable(),

            Images::make('Main image', 'pack'),

            Select::make('Meal pack group', 'meal_pack_groups_id')
                ->options(\App\Models\MealPackGroups::all()->mapWithKeys(function (\App\Models\MealPackGroups $mealPackGroups) {
                    return [$mealPackGroups->getKey() => $mealPackGroups->getTranslation('title', 'is')];
                })->toArray())
                ->displayUsingLabels()
                ->rules('required'),

            Select::make('Count day', 'meal_pack_count_days_id')
                ->options(\App\Models\MealPackCountDays::all()->mapWithKeys(function (\App\Models\MealPackCountDays $mealPackCountDays) {
                    return [$mealPackCountDays->getKey() => $mealPackCountDays->getTranslation('title', 'en')];
                })->toArray())
                ->displayUsingLabels()
                ->rules('required'),

            Translatable::make([
                Textarea::make('Name')
                    ->rules('required', 'max:255')
                    ->displayUsing(function () {
                        return $this->getTranslation('name', 'is');
                    }),

                Textarea::make('Sub title')
                    ->rules('required', 'max:255')
                    ->displayUsing(function () {
                        return $this->getTranslation('sub_title', 'is');
                    }),

                Textarea::make('Short Description')
                    ->rules('required')
                    ->alwaysShow()
                    ->displayUsing(function () {
                        return $this->getTranslation('short_description', 'is');
                    }),

                Textarea::make('Description')
                    ->alwaysShow()
                    ->displayUsing(function () {
                        return $this->getTranslation('description', 'is');
                    }),

                Textarea::make('Ingredients')
                    ->sortable()
                    ->alwaysShow()
                    ->displayUsing(function () {
                        return $this->getTranslation('ingredients', 'is');
                    }),

                Textarea::make('Allergy')
                    ->sortable()
                    ->alwaysShow()
                    ->displayUsing(function () {
                        return $this->getTranslation('allergy', 'is');
                    }),
            ]),

            Currency::make('Price')
                ->min(1)
                ->displayUsing(function ($value) {
                    return "$value kr.";
                })
                ->rules('required')
                ->sortable(),

            Boolean::make('Is price pr Meal?', 'per_type')
                ->hideFromIndex(),

            Number::make('Kcal')
                ->min(0)
                ->displayUsing(function ($value) {
                    return "$value kcal";
                }),

            NestedForm::make('Meal Pack Product'),

            NestedForm::make('Meal Pack Infos'),

            NestedForm::make('Meal Pack Target'),

            Boolean::make('Is Public?', 'is_public'),
        ];
    }

    /**
     * Get the cards available for the request.
     *
     * @param Request $request
     * @return array
     */
    public function cards(Request $request)
    {
        return [];
    }

    /**
     * Get the filters available for the resource.
     *
     * @param Request $request
     * @return array
     */
    public function filters(Request $request)
    {
        return [];
    }

    /**
     * Get the lenses available for the resource.
     *
     * @param Request $request
     * @return array
     */
    public function lenses(Request $request)
    {
        return [];
    }

    /**
     * Get the actions available for the resource.
     *
     * @param Request $request
     * @return array
     */
    public function actions(Request $request)
    {
        return [];
    }
}
