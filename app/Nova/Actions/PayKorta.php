<?php

namespace App\Nova\Actions;

use App\Payment\Korta\Client;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Support\Arr;
use Illuminate\Support\Collection;
use Laravel\Nova\Actions\Action;
use Laravel\Nova\Fields\ActionFields;

class PayKorta extends Action
{
    use InteractsWithQueue, Queueable;

    /**
     * Perform the action on the given models.
     *
     * @param ActionFields $fields
     * @param Collection $models
     * @return mixed
     */
    public function handle(ActionFields $fields, Collection $models)
    {
        $client = app(Client::class);

        $response = [];

        foreach ($models as $order) {
            if ($order->getAttribute('status')->isPaid()) {
                $response = $client->cofPayment($order, $order->getAttribute('amount')->for_payment);
            }
        }

        if (Arr::get($response, 'd39') == 000) {
            return Action::message(__('System has been charged customer successfully'));
        }

        return Action::danger(Arr::get($response, 'd39text'));
    }

    /**
     * Get the fields available on the action.
     *
     * @return array
     */
    public function fields()
    {
        return [];
    }
}
