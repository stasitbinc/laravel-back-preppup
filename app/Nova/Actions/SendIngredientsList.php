<?php

namespace App\Nova\Actions;

use App\Jobs\CheckAndExportIngredientsList;
use Carbon\Carbon;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Support\Collection;
use Laravel\Nova\Actions\Action;
use Laravel\Nova\Fields\ActionFields;

class SendIngredientsList extends Action
{
    use InteractsWithQueue, Queueable;

    protected $value;

    public function __construct(string $value = null)
    {
        $this->value = $value;
    }

    /**
     * Perform the action on the given models.
     *
     * @param ActionFields $fields
     * @param Collection $models
     * @return mixed
     */
    public function handle(ActionFields $fields, Collection $models)
    {
        $date = Carbon::createFromFormat('Y-m-d', $this->value);

        CheckAndExportIngredientsList::dispatch($models, $date);

        return Action::message('Export file successfully sent to your email address. Please wait several minutes.');
    }

    /**
     * Get the fields available on the action.
     *
     * @return array
     */
    public function fields()
    {
        return [];
    }
}
