<?php

namespace App\Nova;

use App\Enums\DayEnum;
use App\Enums\WeekEnum;
use Illuminate\Http\Request;
use Konekt\Enum\Enum;
use Laravel\Nova\Fields\BelongsTo;
use Laravel\Nova\Fields\Boolean;
use Laravel\Nova\Fields\ID;
use Laravel\Nova\Fields\Select;
use Laravel\Nova\Fields\Text;
use Nova\Multiselect\Multiselect;
use R64\NovaFields\Number;

class MealPackProduct extends Resource
{
    /**
     * The model the resource corresponds to.
     *
     * @var string
     */
    public static $model = \App\Models\Pivot\MealPackProduct::class;

    /**
     * The single value that should be used to represent the resource when being displayed.
     *
     * @var string
     */
    public static $title = 'id';

    public static $displayInNavigation = false;

    public static $group = '<span class="hidden">30</span>Meal Packs';

    /**
     * The columns that should be searched.
     *
     * @var array
     */
    public static $search = [
        'id',
    ];

    /**
     * Get the fields displayed by the resource.
     *
     * @param Request $request
     * @return array
     */
    public function fields(Request $request)
    {
        $val = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20];
        $key = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20];

        return [
            ID::make()->sortable(),

            BelongsTo::make('Meal Pack'),

            BelongsTo::make('Product'),

            Number::make('Count')
                ->min(1)
                ->max(5)
                ->sortable(),

//            Multiselect::make('Sending',
//                'sending')->options($data, $selectedOptions),
//            MultiS::make('Sending')->options([
//                '1' => '1',
//                '2' => '2',
//                '3' => '3',
//                '4' => '4',
//                '5' => '5',
//                '6' => '6',
//            ])->hideFromIndex()->rules('required'),
            Text::make('Sending', 'sending')->rules('required'),
            Select::make('Diet')->options([
                'breakfast' => 'Breakfast',
                'dinner' => 'Lunch & dinner',
                'snacks' => 'Snacks & evening snacks',
            ])->hideFromIndex()->rules('required'),

            Select::make('Week')
                ->readonly(true)
                ->options(WeekEnum::choices())
                ->displayUsingLabels()
                ->resolveUsing(function (Enum $value) {
                    return $value->value();
                })->displayUsing(function (Enum $value) {
                    return $value->label();
                })
                ->rules('required'),

            Select::make('Day')
                ->options(DayEnum::choices())
                ->displayUsingLabels()
                ->resolveUsing(function (Enum $value) {
                    return $value->value();
                })->displayUsing(function (Enum $value) {
                    return $value->label();
                })
                ->rules('required'),

            Select::make('Target', 'target_id')
                ->options(\App\Models\Target::all()->mapWithKeys(function (\App\Models\Target $target) {
                    return [$target->getKey() => $target->getTranslation('name', 'is')];
                })->toArray())
                ->displayUsingLabels()
                ->rules('required'),

            Boolean::make('Is Hidden?', 'is_hidden')
                ->help(__('Is hidden for the frontend')),
        ];
    }

    /**
     * Get the cards available for the request.
     *
     * @param Request $request
     * @return array
     */
    public function cards(Request $request)
    {
        return [];
    }

    /**
     * Get the filters available for the resource.
     *
     * @param Request $request
     * @return array
     */
    public function filters(Request $request)
    {
        return [];
    }

    /**
     * Get the lenses available for the resource.
     *
     * @param Request $request
     * @return array
     */
    public function lenses(Request $request)
    {
        return [];
    }

    /**
     * Get the actions available for the resource.
     *
     * @param Request $request
     * @return array
     */
    public function actions(Request $request)
    {
        return [];
    }
}
