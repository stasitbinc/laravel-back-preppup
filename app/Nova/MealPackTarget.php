<?php

namespace App\Nova;

use Illuminate\Http\Request;
use Laravel\Nova\Fields\BelongsTo;
use Laravel\Nova\Fields\ID;
use Laravel\Nova\Fields\Select;
use Laravel\Nova\Fields\Text;
use Laravel\Nova\Http\Requests\NovaRequest;
use R64\NovaFields\JSON;

class MealPackTarget extends Resource
{
    /**
     * The model the resource corresponds to.
     *
     * @var string
     */
    public static $model = \App\Models\MealPackTarget::class;

    /**
     * The single value that should be used to represent the resource when being displayed.
     *
     * @var string
     */
    public static $title = 'id';

    public static $displayInNavigation = false;

    public static $group = '<span class="hidden">30</span>Meal Packs';

    /**
     * The columns that should be searched.
     *
     * @var array
     */
    public static $search = [
        'id',
    ];

    /**
     * Get the fields displayed by the resource.
     *
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function fields(Request $request)
    {
        return [
            ID::make()->sortable(),

            BelongsTo::make('Meal Pack'),

            Select::make('Target', 'target_id')
                ->options(\App\Models\Target::all()->mapWithKeys(function (\App\Models\Target $target) {
                    return [$target->getKey() => $target->getTranslation('name', 'is')];
                })->toArray())
                ->displayUsingLabels()
                ->rules('required'),

            JSON::make('Nutritional', [
                Text::make('Fita')
                    ->rules('required'),

                Text::make('Orka')
                    ->rules('required'),

                Text::make('Protein')
                    ->rules('required'),

                Text::make('Kolvetni')
                    ->rules('required'),
            ])->childConfig([
                'labelClasses' => 'w-1/2 px-8 py-6',
                'fieldClasses' => 'w-full px-8 py-6',
            ]),
        ];
    }

    /**
     * Get the cards available for the request.
     *
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function cards(Request $request)
    {
        return [];
    }

    /**
     * Get the filters available for the resource.
     *
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function filters(Request $request)
    {
        return [];
    }

    /**
     * Get the lenses available for the resource.
     *
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function lenses(Request $request)
    {
        return [];
    }

    /**
     * Get the actions available for the resource.
     *
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function actions(Request $request)
    {
        return [];
    }
}
