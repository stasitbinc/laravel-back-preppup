<?php

namespace App\Nova\Filters;

use App\Enums\OrderStatusEnum;
use Carbon\Carbon;
use Carbon\CarbonPeriod;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;
use Laravel\Nova\Filters\Filter;

class DeliveryDates extends Filter
{
    /**
     * The filter's component.
     *
     * @var string
     */
    public $component = 'select-filter';

    /**
     * Apply the filter to the given query.
     *
     * @param Request $request
     * @param Builder $query
     * @param mixed $value
     * @return Builder
     */
    public function apply(Request $request, $query, $value)
    {
        return $query->whereHas('deliveries', function (Builder $query) use ($value) {
            $query->whereDate('date', $value);
        })->where('status', OrderStatusEnum::PAID);
    }

    /**
     * Get the filter's available options.
     *
     * @param Request $request
     * @return array
     */
    public function options(Request $request)
    {
        $period = collect(CarbonPeriod::create(now(), now()->addMonth()));

        $period = $period->filter(function (Carbon $date) {
            return in_array($date->dayOfWeek, [1, 2, 3, 4, 5, 0]);
        });

        return $period->mapWithKeys(function (Carbon $date) {
            return ["{$date->format('d-m-Y')} ({$date->englishDayOfWeek})" => $date->format('Y-m-d')];
        })->toArray();
    }
}
