<?php

namespace App\Nova\Filters;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;
use Laravel\Nova\Filters\BooleanFilter;

class UserOrders extends BooleanFilter
{
    /**
     * The filter's component.
     *
     * @var string
     */
    public $component = 'select-filter';

    /**
     * Apply the filter to the given query.
     *
     * @param Request $request
     * @param Builder $query
     * @param mixed $value
     * @return Builder
     */
    public function apply(Request $request, $query, $value)
    {
        switch ($value) {
            case 'not_ordered':
                return $query->whereDoesntHave('orders');
            case 'once':
                return $query->has('orders', 1);
            default:
                return $query;
        }
    }

    /**
     * Get the filter's available options.
     *
     * @param Request $request
     * @return array
     */
    public function options(Request $request)
    {
        return [
            __('Registered not ordered') => 'not_ordered',
            __('Ordered only once') => 'once',
        ];
    }

    /**
     * The default value of the filter.
     *
     * @return string
     * @var string
     */
    public function default()
    {
        return '';
    }
}
