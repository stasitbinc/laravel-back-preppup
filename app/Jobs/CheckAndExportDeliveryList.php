<?php

namespace App\Jobs;

use App\Exports\OrdersDeliveryExport;
use App\Mail\DeliveryListExport;
use App\Models\Order;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Mail;
use Maatwebsite\Excel\Excel as BaseExcel;
use Maatwebsite\Excel\Facades\Excel;

class CheckAndExportDeliveryList implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $orders;

    protected $date;

    /**
     * Create a new job instance.
     *
     * @param Collection $orders
     * @param Carbon|null $date
     */
    public function __construct(Collection $orders, Carbon $date = null)
    {
        $this->orders = $orders;

        $this->date = $date;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        if ($this->date) {
            $this->exportDeliveryList($this->date);
        } else {
            $this->exportDeliveryList(now()->addDays(2));

            /*Carbon::setLocale('en');

            switch (now()->format('l')) {
                case 'Wednesday':
                case 'Monday':
                    $this->exportDeliveryList(now()->addDays(3));
                    break;
                case 'Friday':
                    $this->exportDeliveryList(now()->addDays(4));
                    break;
            }*/
        }
    }

    public function exportDeliveryList(Carbon $date)
    {
        $this->orders = $this->orders->filter(function (Order $order) use ($date) {
            return $order->deliveries()->whereDate('date', $date->format('Y-m-d'))->exists() && $order->getAttribute('postcode_id');
        });

        if ($this->orders->isNotEmpty()) {
            $filename = "Delivery List ({$date->format('l d.F')}).xlsx";

            $attachment = Excel::raw(new OrdersDeliveryExport($this->orders, $date), BaseExcel::XLSX);

//            Storage::disk('google')->put($filename, $attachment);

            $users = User::query()->role('Admin')->get();

            $users->map(function (User $user) use ($attachment, $filename, $date) {
                Mail::to($user)->send(new DeliveryListExport($attachment, $filename, $date));
            });
        }
    }
}
