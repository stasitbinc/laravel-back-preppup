<?php

namespace App\Traits;

use App\Models\Ingredient;
use App\Models\Target;

trait HasCalculatedSize
{
    public function getSizesAttribute()
    {
        $targets = $this->targets()->groupBy('target_id')->get();

        $targets = $targets->mapWithKeys(function (Target $target) {
            $ingredients = $this->product_ingredients()
                ->where('product_target.target_id', $target->getKey())
                ->get();

            $ingredients = $ingredients->map(function (Ingredient $ingredient) {
                $multiplier = $ingredient->pivot->size / 100;

                $ingredient->setAttribute('calories', $ingredient->getAttribute('calories') * $multiplier);
                $ingredient->setAttribute('fat', $ingredient->getAttribute('fat') * $multiplier);
                $ingredient->setAttribute('carbohydrates', $ingredient->getAttribute('carbohydrates') * $multiplier);
                $ingredient->setAttribute('protein', $ingredient->getAttribute('protein') * $multiplier);

                return $ingredient;
            });

            return [$target->getAttribute('name') => [
                'calories' => round($ingredients->sum('calories'), 2),
                'fat' => round($ingredients->sum('fat'), 2),
                'carbohydrates' => round($ingredients->sum('carbohydrates'), 2),
                'protein' => round($ingredients->sum('protein'), 2),
                'target_id' => $target->getKey(),
            ]];
        });

        return $targets;
    }
}
