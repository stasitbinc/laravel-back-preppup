<?php

namespace App\Payment\Pei;

class Config
{
    private $authUrl;
    private $orderUrl;
    private $paymentUrl;
    private $clientId;
    private $secretKey;
    private $merchantId;

    /**
     * Config constructor.
     *
     * @param $authUrl
     * @param $orderUrl
     * @param $paymentUrl
     * @param $clientId
     * @param $secretKey
     * @param $merchantId
     */
    public function __construct($authUrl, $orderUrl, $paymentUrl, $clientId, $secretKey, $merchantId)
    {
        $this->authUrl = $authUrl;
        $this->orderUrl = $orderUrl;
        $this->paymentUrl = $paymentUrl;
        $this->clientId = $clientId;
        $this->secretKey = $secretKey;
        $this->merchantId = $merchantId;
    }

    /**
     * @return mixed
     */
    public function getAuthUrl()
    {
        return $this->authUrl;
    }

    /**
     * @return mixed
     */
    public function getOrderUrl()
    {
        return $this->orderUrl;
    }

    /**
     * @return mixed
     */
    public function getPaymentUrl()
    {
        return $this->paymentUrl;
    }

    /**
     * @return mixed
     */
    public function getClientId()
    {
        return $this->clientId;
    }

    /**
     * @return mixed
     */
    public function getSecretKey()
    {
        return $this->secretKey;
    }

    /**
     * @return mixed
     */
    public function getMerchantId()
    {
        return $this->merchantId;
    }
}
