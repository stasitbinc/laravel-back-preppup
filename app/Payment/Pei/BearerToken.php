<?php

namespace App\Payment\Pei;

class BearerToken
{
    private $accessToken;

    private $expiresIn;

    public function __construct($accessToken, $expiresIn)
    {
        $this->accessToken = $accessToken;

        $this->expiresIn = $expiresIn;
    }

    /**
     * @return mixed
     */
    public function getExpiresIn()
    {
        return $this->expiresIn;
    }

    /**
     * @return mixed
     */
    public function getAccessToken()
    {
        return $this->accessToken;
    }

    public function __toString()
    {
        return 'Bearer '.$this->getAccessToken();
    }
}
