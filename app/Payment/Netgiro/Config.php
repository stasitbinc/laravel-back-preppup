<?php

namespace App\Payment\Netgiro;

class Config
{
    private $url;
    private $appId;
    private $secretKey;
    private $nonce;

    /**
     * Config constructor.
     *
     * @param $url
     * @param $appId
     * @param $secretKey
     */
    public function __construct($url, $appId, $secretKey)
    {
        $this->url = $url;
        $this->appId = $appId;
        $this->secretKey = $secretKey;
        $this->nonce = time() * 1000;
    }

    /**
     * @return mixed
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * @return mixed
     */
    public function getAppId()
    {
        return $this->appId;
    }

    /**
     * @return mixed
     */
    public function getSecretKey()
    {
        return $this->secretKey;
    }

    /**
     * @return mixed
     */
    public function getNonce()
    {
        return $this->nonce;
    }
}
