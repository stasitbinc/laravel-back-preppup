<?php

namespace App\Payment\Korta;

use App\Http\Requests\PaymentRequest;
use App\Models\Order;
use Carbon\Carbon;
use GuzzleHttp\Client as Guzzle;
use GuzzleHttp\HandlerStack;

class Client
{
    /**
     * @var Guzzle
     */
    private $client;

    /**
     * @var Config
     */
    private $config;

    public function __construct(Guzzle $client, Config $config)
    {
        $this->client = $client;
        $this->config = $config;
    }

    /**
     * @param Config $config
     * @return Client
     */
    public static function create(Config $config)
    {
        $stack = HandlerStack::create();

        $client = new Guzzle([
            'base_uri' => $config->getEndpoint(),
            'headers' => [
                'Accept' => 'application/json',
                'Content-Type' => 'application/json',
            ],
            'handler' => $stack,
            'timeout' => 10000,
        ]);

        return new static($client, $config);
    }

    public function voidWebPayment(Order $order, PaymentRequest $request)
    {
        return $this->request('RequestReversal', [
            'form_params' => [
                'user' => $this->config->getUser(),
                'pwd' => $this->config->getPassword(),
                'd41' => $this->config->getTerminal(),
                'd42' => $this->config->getMerchant(),
                'd4' => "{$order->getAttribute('amount')->for_payment}00",
                'de4' => '2',
                'd49' => '352',
                'd12' => Carbon::parse($request->get('time'))->format('ymdHis'),
                'd31' => $order->getAttribute('uuid'),
                'd38' => $request->get('authcode'),
            ],
        ], 'POST');
    }

    public function cofPayment(Order $order, $price)
    {
        return $this->request('RequestAuthorisation', [
            'form_params' => [
                'user' => $this->config->getUser(),
                'pwd' => $this->config->getPassword(),
                'd4' => "{$price}00",
                'de4' => '2',
                'd31' => $order->getAttribute('uuid'),
                'd41' => $this->config->getTerminal(),
                'd42' => $this->config->getMerchant(),
                'd49' => '352',
                'capture' => 'True',
                'daskm' => (string) $order->getAttribute('payment')->korta->token,
//                'initiatedby' => 'M',
//                'cof' => 'U',
//                'transactionId' => (string)$order->getAttribute('payment')->korta->transaction,
                'd22cp' => 'RPA',
            ],
        ], 'POST');
    }

    protected function request($endpoint, $params = [], $method = 'GET')
    {
        $response = $this->client->request($method, $endpoint, $params);

        parse_str($response->getBody()->getContents(), $output);

        return $output;
    }
}
