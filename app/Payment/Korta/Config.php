<?php

namespace App\Payment\Korta;

class Config
{
    private $endpoint;
    private $user;
    private $pwd;
    private $terminal;
    private $merchant;

    /**
     * Config constructor.
     *
     * @param $endpoint
     * @param $user
     * @param $pwd
     * @param $terminal
     * @param $merchant
     */
    public function __construct($endpoint, $user, $pwd, $terminal, $merchant)
    {
        $this->endpoint = $endpoint;
        $this->user = $user;
        $this->pwd = $pwd;
        $this->terminal = $terminal;
        $this->merchant = $merchant;
    }

    /**
     * @return mixed
     */
    public function getEndpoint()
    {
        return $this->endpoint;
    }

    /**
     * @return mixed
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @return mixed
     */
    public function getPassword()
    {
        return $this->pwd;
    }

    /**
     * @return mixed
     */
    public function getTerminal()
    {
        return $this->terminal;
    }

    /**
     * @return mixed
     */
    public function getMerchant()
    {
        return $this->merchant;
    }
}
