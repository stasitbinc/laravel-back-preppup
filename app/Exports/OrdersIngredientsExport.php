<?php

namespace App\Exports;

use Carbon\Carbon;
use Illuminate\Contracts\View\View;
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;

class OrdersIngredientsExport implements FromView, ShouldAutoSize
{
    use Exportable;

    protected $meals;

    protected $date;

    public function __construct(Collection $meals, Carbon $date)
    {
        $this->meals = $meals;

        $this->date = $date;
    }

    /*public function columnWidths(): array
    {
        return [
            'A' => 30,
            'B' => 20,
            'C' => 20,
        ];
    }*/

    /**
     * @return View
     */
    public function view(): View
    {
        return view('exports.ingredients', [
            'meals' => $this->meals,
            'date' => $this->date,
        ]);
    }
}
