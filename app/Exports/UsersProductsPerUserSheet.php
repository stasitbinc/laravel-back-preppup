<?php

namespace App\Exports;

use Carbon\Carbon;
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\WithTitle;
use Maatwebsite\Excel\Events\AfterSheet;
use PhpOffice\PhpSpreadsheet\Style\Alignment;
use PhpOffice\PhpSpreadsheet\Style\Fill;

class UsersProductsPerUserSheet implements WithEvents, FromCollection, WithMapping, WithHeadings, ShouldAutoSize, WithTitle
{
    protected $products;

    protected $date;

    protected $user;

    public function __construct(Collection $products, Carbon $date, string $user)
    {
        $this->products = $products;

        $this->date = $date;

        $this->user = $user;
    }

    public function title(): string
    {
        return iconv('UTF-8', 'ASCII//TRANSLIT', str_replace('/', '-', $this->user));
    }

    public function collection()
    {
        return $this->products;
    }

    public function headings(): array
    {
        return [
            'Máltíð'.PHP_EOL.'Meal',
            'Samtals'.PHP_EOL.'Total',
        ];
    }

    public function map($product): array
    {
        return [
            "{$product->getTranslation('name', 'is')} ({$product->target->getTranslation('name', 'is')})",
            $product->getAttribute('total') ?? null,
        ];
    }

    public function registerEvents(): array
    {
        return [
            AfterSheet::class => function (AfterSheet $event) {
                $event->sheet->styleCells(
                    'A1:B1',
                    [
                        'font' => [
                            'size' => 15,
                            'color' => ['argb' => 'ffffff'],
                        ],
                        'fill' => [
                            'fillType' => Fill::FILL_SOLID,
                            'color' => ['argb' => '3b7b1e'],
                        ],
                        'alignment' => [
                            'horizontal' => Alignment::HORIZONTAL_CENTER,
                            'vertical' => Alignment::VERTICAL_CENTER,
                        ],
                    ]
                );

                $rowNumber = $this->products->count() + 2;

                $event->sheet->insertNewRowBefore($rowNumber, 1);

                $event->sheet->setCellValue("A$rowNumber", 'Samtals:');

                $event->sheet->setCellValue("B$rowNumber", $this->products->sum('total'));

                $event->sheet->styleCells(
                    "A$rowNumber:B$rowNumber",
                    [
                        'fill' => [
                            'fillType' => Fill::FILL_SOLID,
                            'color' => ['argb' => 'dddddd'],
                        ],
                    ]
                );
            },
        ];
    }
}
