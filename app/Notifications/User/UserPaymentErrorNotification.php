<?php

namespace App\Notifications\User;

use App\Models\Order;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class UserPaymentErrorNotification extends Notification implements ShouldQueue
{
    use Queueable;

    private $order;

    /**
     * Create a new notification instance.
     *
     * @param Order $order
     */
    public function __construct(Order $order)
    {
        $this->order = $order;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param mixed $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param mixed $notifiable
     * @return MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
            ->error()
            ->greeting(__('Hi, :first_name!', ['first_name' => $notifiable->getAttribute('first_name')]))
            ->subject(__('Prepp Up was unable to charge you and finish your order.'))
            ->line(__('Prepp Up was unable to charge you and finish your order.'))
            ->line(__('Order number: :order_number', ['order_number' => $this->order->getAttribute('uuid')]))
            ->line(__('Payment Method: :payment_method', ['payment_method' => ucfirst($this->order->getAttribute('payment')->type)]))
            ->line(__('Please login to your Prepp Up account and try a different payment method.'));
    }

    /**
     * Get the array representation of the notification.
     *
     * @param mixed $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
