<?php

namespace App\Notifications;

use App\Models\Order;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;
use Illuminate\Support\Arr;

class PaymentNotification extends Notification implements ShouldQueue
{
    use Queueable;

    private $response;

    private $order;

    /**
     * Create a new notification instance.
     *
     * @param array $response
     * @param Order $order
     */
    public function __construct(array $response, Order $order)
    {
        $this->response = $response;

        $this->order = $order;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param mixed $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param mixed $notifiable
     * @return MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
            ->subject(__('Payment Info'))
            ->line(__('System has been charged customer successfully'))
            ->line(__('User name: :name', ['name' => $this->order->user()->first()->getAttribute('name')]))
            ->line(__('Order number: :order_number', ['order_number' => $this->order->getAttribute('uuid')]))
            ->line(__('Payment Method: :payment_method', ['payment_method' => ucfirst($this->order->getAttribute('payment')->type)]))
            ->line(Arr::get($this->response, 'd2brand', null).' '.Arr::get($this->response, 'd2dsp', null))
            ->line(Arr::get($this->response, 'errortext', null))
            ->line(Arr::get($this->response, 'correlationId', null));
    }

    /**
     * Get the array representation of the notification.
     *
     * @param mixed $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
