<?php

namespace App\Mail;

use Carbon\Carbon;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class UsersProductsExport extends Mailable
{
    use Queueable, SerializesModels;

    private $attachment;

    private $filename;

    private $date;

    /**
     * Create a new message instance.
     *
     * @param $attachment
     * @param string $filename
     * @param Carbon $date
     */
    public function __construct($attachment, string $filename, Carbon $date)
    {
        $this->attachment = $attachment;

        $this->filename = $filename;

        $this->date = $date;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->markdown('emails.orders.products')
            ->subject("Clients Meals ({$this->date->format('l d.F')})")
            ->attachData($this->attachment, $this->filename);
    }
}
