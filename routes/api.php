<?php

use Illuminate\Support\Facades\Route;

Route::get('/pei/confirm', 'OrderController@netgiroConfirm');

Route::prefix('meal-packs')
    ->group(function () {
        Route::get('/', 'MealPackController@index');
        Route::get('/{meal_pack}', 'MealPackController@show');
        Route::get('/search/group', 'MealPackController@showGroup');
        Route::get('/group', 'MealPackController@group');
        Route::get('/days', 'MealPackController@days');
    });

Route::prefix('meal-days')
    ->group(function () {
        Route::get('/', 'MealPackController@days');
    });

Route::prefix('meal-group')
    ->group(function () {
        Route::get('/', 'MealPackController@group');
        Route::get('/search', 'MealPackController@search');
    });

Route::prefix('posts')
    ->group(function () {
        Route::get('/', 'PostController@index');
    });

Route::prefix('terms')
    ->group(function () {
        Route::get('/', 'TermsController@index');
    });

Route::prefix('contact')
    ->group(function () {
        Route::post('/', 'ContactUsController@store');
    });

Route::prefix('products')
    ->group(function () {
        Route::get('/', 'ProductController@index');
        Route::get('/testProduct', 'ProductController@testProduct');
    });

Route::prefix('product-types')
    ->group(function () {
        Route::get('/', 'ProductTypeController@index');
    });

Route::prefix('faq-categories')
    ->group(function () {
        Route::get('/', 'FaqCategoryController@index');
    });

Route::prefix('targets')
    ->group(function () {
        Route::get('/', 'TargetController@index');
    });

Route::prefix('allergies')
    ->group(function () {
        Route::get('/', 'AllergiesController@index');
    });

Route::prefix('user')
    ->group(function () {
        Route::post('/register', 'UserController@store');
        Route::post('/forgot', 'UserController@forgot');
        Route::post('/subscribe', 'UserController@subscribe');
    });

Route::prefix('subscription-plans')
    ->group(function () {
        Route::get('/', 'SubscriptionPlansController@index');
    });

Route::prefix('postcodes')
    ->group(function () {
        Route::get('/', 'PostcodeController@index');
        Route::get('/{postcode}', 'PostcodeController@show');
    });

Route::prefix('cart')
    ->group(function () {
        Route::get('/{cart?}', 'CartController@show');
        Route::post('/{cart?}', 'CartController@store');
        Route::delete('/{cart_item}', 'CartController@destroy');
        Route::put('/{cart_item}/increase', 'CartController@increase');
        Route::put('/{cart_item}/decrease', 'CartController@decrease');
    });

Route::group(['middleware' => 'auth:api'], function () {
    Route::prefix('user')
        ->group(function () {
            Route::get('/', 'UserController@show');
            Route::post('/logout', 'UserController@logout');
            Route::put('/', 'UserController@update');
            Route::put('/language', 'UserController@language');
        });

    Route::prefix('orders')
        ->group(function () {
            Route::get('/', 'OrderController@index');
            Route::put('/{order}', 'OrderController@updateStep');
            Route::post('/{order}/complete', 'OrderController@complete');
            Route::put('/{order}/check-and-pay', 'OrderController@checkAndPay');
            Route::get('/{order}', 'OrderController@show');
            Route::delete('/{order}', 'OrderController@destroy');
            Route::post('/check/{order?}', 'OrderController@checkStep');
            Route::post('/coupon', 'OrderController@checkCoupon');
            Route::get('/{order}/pei-url', 'OrderController@getPaiPaymentUrl');
            Route::put('/{order}/check-and-pay-pei', 'OrderController@checkAndPayPei');
        });

    Route::prefix('subscriptions')
        ->group(function () {
            Route::get('/', 'SubscriptionController@index');
            Route::put('/{subscription}', 'SubscriptionController@cancel');
        });
});
